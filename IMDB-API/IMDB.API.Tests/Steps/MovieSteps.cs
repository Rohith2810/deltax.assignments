﻿using IMDB.API.Tests.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;

namespace IMDB.API.Tests.Steps
{
	[Scope(Feature ="Movie Resource")]
	[Binding]
	public class MovieSteps : BaseSteps
	{
		public MovieSteps(CustomWebApplicationFactory<TestStartup> factory)
			:base(factory.WithWebHostBuilder(builder=>
			{
				builder.ConfigureServices(services =>
				{
					services.AddScoped(services => MovieMock.movieRepoMock.Object);
					services.AddScoped(services => ActorMock.actorRepoMock.Object);
					services.AddScoped(services => ProducerMock.producerRepoMock.Object);
					services.AddScoped(services => GenreMock.genreRepoMock.Object);
				});
			}))
		{

		}

		[BeforeScenario]
		public void MockRepositories()
		{
			MovieMock.MockGetAll();
			MovieMock.MockGetById();
			MovieMock.MockGetByName();
			MovieMock.MockAdd();
			MovieMock.MockPartialUpdate();
			MovieMock.MockUpdate();
			MovieMock.MockRemove();
			ProducerMock.MockGetById();
			ActorMock.MockGetByMovieId();
			GenreMock.MockGetByMovieId();
		}
	}
}
