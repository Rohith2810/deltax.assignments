﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using IMDB.API.Tests.MockResources;
using TechTalk.SpecFlow;

namespace IMDB.API.Tests.Steps
{
	[Scope(Feature ="Genre Resource")]
	[Binding]
	public class GenreSteps : BaseSteps
	{
		public GenreSteps(CustomWebApplicationFactory<TestStartup> factory)
			: base(factory.WithWebHostBuilder(builder => 
			{
				builder.ConfigureServices(services =>
				{
					services.AddScoped(services => GenreMock.genreRepoMock.Object);
				});
			}))
		{

		}

		[BeforeScenario]
		public void MockRepositories()
		{
			GenreMock.MockGetAll();
			GenreMock.MockGetById();
			GenreMock.MockAdd();
			GenreMock.MockPartialUpdate();
			GenreMock.MockUpdate();
			GenreMock.MockRemove();
		}
	}
}
