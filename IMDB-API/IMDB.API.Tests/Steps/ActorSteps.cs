﻿using System;
using IMDB.API.Tests.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;

namespace IMDB.API.Tests.Steps
{
	[Scope(Feature = "Actor Resource")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
		public ActorSteps(CustomWebApplicationFactory<TestStartup> factory)
			:base(factory.WithWebHostBuilder(builder=> {
				builder.ConfigureServices(services =>
				{
					services.AddScoped(services => ActorMock.actorRepoMock.Object);
				});
			}))
		{

		}

		[BeforeScenario]
		public void MockRepositories()
		{
			ActorMock.MockGetAll();
			ActorMock.MockGetById();
			ActorMock.MockAdd();
			ActorMock.MockPartialUpdate();
			ActorMock.MockUpdate();
			ActorMock.MockRemove();
		}
	}
}
