﻿Feature: Producer Resource
Background: 
	Given I am a client

@GetAllProducers
Scenario: Get all Producers
	When I make GET Request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | status code | response                                                                                                                                                               |
	| /producers | 200         | [{"id":1,"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"},{"id":2,"name":"Nicole","bio":"British","dob":"1975-09-02T00:00:00","sex":"Male"}] |

@GetProducerById
Scenario: Get Producer by Id
	When I make GET Request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route         | status code | response                                                                          |
	| /producers/2  | 200         | {"id":2,"name":"Nicole","bio":"British","dob":"1975-09-02T00:00:00","sex":"Male"} |
	| /producers/10 | 404         | No Producer is present with given id '10'                                         |

@AddProducer
Scenario: Add Producer
	When I make POST request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | request                                                                      | status code | response                                             |
	| /producers | {"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"}  | 200         | {"id":1}                                             |
	| /producers | {"name":"","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"}        | 400         | Producer name can't be null or empty                 |
	| /producers | {"name":"Zeshan","bio":"","dob":"1979-03-02T00:00:00","sex":"Male"}          | 400         | Producer bio can't be null or empty                  |
	| /producers | {"name":"Zeshan","bio":"American","dob":"2021-05-02T00:00:00","sex":"Male"}  | 400         | Producer date of birth should be before current date |
	| /producers | {"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":""}      | 400         | Producer sex can't be null or empty                  |
	| /producers | {"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"hello"} | 400         | Producer sex is invalid                              |

@PartialUpdateProducer
Scenario: Partial Update Producer
    When I make PATCH request '<route>' with following data '<request>'
    Then response code must be '<status code>'
    And response data must look like '<response>'
    Examples: 
    | route         | request                                                                                                      | status code | response                                             |
    | /producers/2  | [{"propertyName":"Name","propertyValue":"Nicole William"},{"propertyName":"Bio","propertyValue":"American"}] | 200         | {"id":2}                                             |
    | /producers/2  | [{"propertyName":"","propertyValue":"Nicole"}]                                                               | 400         | Property name can't be null or empty                 |
    | /producers/2  | [{"propertyName":"Name","propertyValue":""}]                                                                 | 400         | Property value can't be null or empty                |
    | /producers/2  | [{"propertyName":"DOB","propertyValue":"2021-10-28"}]                                                        | 400         | Producer date of birth should be before current date |
    | /producers/2  | [{"propertyName":"DOB","propertyValue":"1999-10-"}]                                                          | 400         | Producer date of birth should be a valid date        |
    | /producers/2  | [{"propertyName":"Sex","propertyValue":"Hello"}]                                                             | 400         | Producer sex is invalid                              |
    | /producers/2  | [{"propertyName":"Hello","propertyValue":"Nicole"}]                                                          | 400         | Invalid property name 'Hello'                        |
    | /producers/10 | [{"propertyName":"Name","propertyValue":"Mila"}]                                                             | 404         | No Producer is present with given id '10'            |
    | /producers/2  | [{"propertyName":"Name","propertyValue":"Mila"},{"propertyName":"Name","propertyValue":"Kunis"}]             | 400         | A property is given more than once                   |

@UpdateProducer
Scenario: Update Producer
	When I make PUT request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route         | request                                                                      | status code | response                                             |
	| /producers/1  | {"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"}  | 200         | {"id":1}                                             |
	| /producers/10 | {"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"}  | 404         | No Producer is present with given id '10'            |
	| /producers/1  | {"name":"","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"}        | 400         | Producer name can't be null or empty                 |
	| /producers/1  | {"name":"Zeshan","bio":"","dob":"1979-03-02T00:00:00","sex":"Male"}          | 400         | Producer bio can't be null or empty                  |
	| /producers/1  | {"name":"Zeshan","bio":"American","dob":"2021-05-02T00:00:00","sex":"Male"}  | 400         | Producer date of birth should be before current date |
	| /producers/1  | {"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":""}      | 400         | Producer sex can't be null or empty                  |
	| /producers/1  | {"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"hello"} | 400         | Producer sex is invalid                              |

@RemoveProducer
Scenario: Remove Producer
	When I make DELETE request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route         | status code | response                                  |
	| /producers/2  | 200         | {"id":2}                                  |
	| /producers/10 | 404         | No Producer is present with given id '10' |