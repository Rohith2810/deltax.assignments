﻿Feature: Genre Resource
Background: 
	Given I am a client

@GetAllGenres
Scenario: Get all Genres
	When I make GET Request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route   | status code | response                                            |
	| /genres | 200         | [{"id":1,"name":"Action"},{"id":2,"name":"Sci-Fi"}] |

@GetGenreById
Scenario: Get Genre by Id
	When I make GET Request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | status code | response                               |
	| /genres/2  | 200         | {"id":2,"name":"Sci-Fi"}               |
	| /genres/10 | 404         | No Genre is present with given id '10' |

@AddGenre
Scenario: Add Genre
	When I make POST request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route   | request           | status code | response                          |
	| /genres | {"name":"Action"} | 200         | {"id":1}                          |
	| /genres | {"name":""}       | 400         | Genre name can't be null or empty |

@PartialUpdateGenre
Scenario: Partial Update Genre
	When I make PATCH request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | request                                                                                               | status code | response                               |
	| /genres/1  | [{"propertyName":"Name","propertyValue":"Thriller"}]                                                  | 200         | {"id":1}                               |
	| /genres/1  | [{"propertyName":"","propertyValue":"Thriller"}]                                                      | 400         | Property name can't be null or empty   |
	| /genres/1  | [{"propertyName":"Name","propertyValue":""}]                                                          | 400         | Property value can't be null or empty  |
	| /genres/10 | [{"propertyName":"Name","propertyValue":"Thriller"}]                                                  | 404         | No Actor is present with given id '10' |
	| /genres/1  | [{"propertyName":"Hello","propertyValue":"Thriller"}]                                                 | 400         | Invalid property name 'Hello'          |
	| /genres/2  | [{"propertyName":"Name","propertyValue":"Thriller"},{"propertyName":"Name","propertyValue":"SCI-FI"}] | 400         | A property is given more than once     |

@UpdateGenre
Scenario: Update Genre
	When I make PUT request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | request             | status code | response                               |
	| /genres/1  | {"name":"Thriller"} | 200         | {"id":1}                               |
	| /genres/10 | {"name":"Thriller"} | 404         | No Genre is present with given id '10' |
	| /genres/1  | {"name":""}         | 400         | Genre name can't be null or empty      |

@RemoveGenre
Scenario: Remove Genre
	When I make DELETE request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | status code | response                               |
	| /genres/2  | 200         | {"id":2}                               |
	| /genres/10 | 404         | No Genre is present with given id '10' |