﻿Feature: Movie Resource
Background: 
	Given I am a client

@GetAllMovies
Scenario: Get All Movies
	When I make GET Request '<route>'
	Then response code must be '<statusCode>'
	And response data must look like '<response>'
	Examples: 
	| route   | statusCode | response                                                                                                                                                                                                                                                                                                                                                                                                                                               |
	| /movies | 200        | [{"id":1,"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producer":{"id":1,"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"},"actors":[{"id":1,"name":"Christian Bale","bio":"British","dob":"1979-03-02T00:00:00","sex":"Male"},{"id":2,"name":"Mila Kunis","bio":"Ukranian","dob":"1973-06-22T00:00:00","sex":"Female"}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"Sci-Fi"}]}] |

@GetMovieById
Scenario: Get Movie by Id
	When I make GET Request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | status code | response                                                                                                                                                                                                                                                                                                                                                                                                                                             |
	| /movies/1  | 200         | {"id":1,"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producer":{"id":1,"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"},"actors":[{"id":1,"name":"Christian Bale","bio":"British","dob":"1979-03-02T00:00:00","sex":"Male"},{"id":2,"name":"Mila Kunis","bio":"Ukranian","dob":"1973-06-22T00:00:00","sex":"Female"}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"Sci-Fi"}]} |
	| /movies/10 | 404         | No Movie is present with given id '10'                                                                                                                                                                                                                                                                                                                                                                                                               |

@GetMovieByName
Scenario: Get Movie by Name
	When I make GET Request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route                           | status code | response                                                                                                                                                                                                                                                                                                                                                                                                                                             |
	| /movies/getbyname?name=Terminal | 200         | {"id":1,"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producer":{"id":1,"name":"Zeshan","bio":"American","dob":"1979-03-02T00:00:00","sex":"Male"},"actors":[{"id":1,"name":"Christian Bale","bio":"British","dob":"1979-03-02T00:00:00","sex":"Male"},{"id":2,"name":"Mila Kunis","bio":"Ukranian","dob":"1973-06-22T00:00:00","sex":"Female"}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"Sci-Fi"}]} |
	| /movies/getbyname?name=Hello    | 404         | No Movie is present with given name 'Hello'                                                                                                                                                                                                                                                                                                                                                                                                          |

@AddMovie
Scenario: Add Movie
	When I make POST request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route   | request                                                                                                                                | status code | response                                 |
	| /movies | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}  | 200         | {"id":1}                                 |
	| /movies | {"name":"","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}          | 400         | Movie name can't be null or empty        |
	| /movies | {"name":"Terminal","yearOfRelease":0,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}     | 400         | Movie year of release should not be zero |
	| /movies | {"name":"Terminal","yearOfRelease":2006,"plot":"","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}                  | 400         | Movie plot can't be null or empty        |
	| /movies | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}     | 400         | Movie poster url can't be null or empty  |
	| /movies | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":10,"actorIds":[1,2],"genreIds":[1,2]} | 400         | Producer Id is not present               |
	| /movies | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[10,2],"genreIds":[1,2]} | 400         | Actor Id(s) are not present              |
	| /movies | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[10,2]} | 400         | Genre Id(s) are not present              |

@PartialUpdateMovie
Scenario: Partial Update Movie
	When I make PATCH request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | request                                                                                                      | status code | response                               |
	| /movies/1  | [{"propertyName":"Name","propertyValue":"Terminal 2"},{"propertyName":"Plot","propertyValue":"American"}]    | 200         | {"id":1}                               |
	| /movies/1  | [{"propertyName":"","propertyValue":"Terminal 2"}]                                                           | 400         | Property name can't be null or empty   |
	| /movies/1  | [{"propertyName":"Name","propertyValue":""}]                                                                 | 400         | Property value can't be null or empty  |
	| /movies/1  | [{"propertyName":"YearOfRelease","propertyValue":"hello"}]                                                   | 400         | YearOfRelease must be an integer       |
	| /movies/1  | [{"propertyName":"ProducerId","propertyValue":"hello"}]                                                      | 400         | ProducerId must be an integer          |
	| /movies/1  | [{"propertyName":"Hello","propertyValue":"Mila"}]                                                            | 400         | Invalid property name 'Hello'          |
	| /movies/10 | [{"propertyName":"Name","propertyValue":"Terminal 5"}]                                                       | 404         | No Movie is present with given id '10' |
	| /movies/1  | [{"propertyName":"Name","propertyValue":"Termianal 2"},{"propertyName":"Name","propertyValue":"Terminal 2"}] | 400         | A property is given more than once     |
	| /movies/1  | [{"propertyName":"ProducerId","propertyValue":"10"}]                                                         | 400         | Producer Id is not present             |
	| /movies/1  | [{"propertyName":"ActorIds","propertyValue":"1,10"}]                                                         | 400         | Actor Id(s) are not present            |
	| /movies/1  | [{"propertyName":"GenreIds","propertyValue":"9,10"}]                                                         | 400         | Genre Id(s) are not present            |

@UpdateMovie
Scenario: Update Movie
	When I make PUT request '<route>' with following data '<request>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | request                                                                                                                                | status code | response                                 |
	| /movies/1  | {"name":"Terminal","yearOfRelease":2005,"plot":"An spanish movie","posterURL":"URL","producerId":2,"actorIds":[1,2],"genreIds":[1,2]}  | 200         | {"id":1}                                 |
	| /movies/10 | {"name":"Terminal","yearOfRelease":2005,"plot":"An spanish movie","posterURL":"URL","producerId":2,"actorIds":[1,2],"genreIds":[1,2]}  | 404         | Movie is not present with given id '10'  |
	| /movies/1  | {"name":"","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}          | 400         | Movie name can't be null or empty        |
	| /movies/1  | {"name":"Terminal","yearOfRelease":0,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}     | 400         | Movie year of release should not be zero |
	| /movies/1  | {"name":"Terminal","yearOfRelease":2006,"plot":"","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}                  | 400         | Movie plot can't be null or empty        |
	| /movies/1  | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"","producerId":1,"actorIds":[1,2],"genreIds":[1,2]}     | 400         | Movie poster url can't be null or empty  |
	| /movies/1  | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":10,"actorIds":[1,2],"genreIds":[1,2]} | 400         | Producer Id is not present               |
	| /movies/1  | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[10,2],"genreIds":[1,2]} | 400         | Actor Id(s) are not present              |
	| /movies/1  | {"name":"Terminal","yearOfRelease":2006,"plot":"An english movie","posterURL":"URL","producerId":1,"actorIds":[1,2],"genreIds":[10,2]} | 400         | Genre Id(s) are not present              |

@DeleteMovie
Scenario: Delete Movie
	When I make DELETE request '<route>'
	Then response code must be '<status code>'
	And response data must look like '<response>'
	Examples: 
	| route      | status code | response                               |
	| /movies/1  | 200         | {"id":1}                               |
	| /movies/10 | 404         | No Movie is present with given id '10' |