﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Moq;

namespace IMDB.API.Tests.MockResources
{
	public class MovieMock
	{
		public static readonly Mock<IMovieRepository> movieRepoMock = new Mock<IMovieRepository>();
		public static readonly Mock<IActorRepository> actorRepoMock = new Mock<IActorRepository>();
		public static readonly Mock<IProducerRepository> producerRepoMock = new Mock<IProducerRepository>();
		public static readonly Mock<IGenreRepository> genreRepoMock = new Mock<IGenreRepository>();

		public static void MockGetAll()
		{
			movieRepoMock.Setup(repo => repo.GetAll()).Returns(ListAllMovies());
		}

		public static void MockGetById()
		{
			movieRepoMock.Setup(repo => repo.GetById(It.IsAny<int>()))
				.Returns((int id) => ListAllMovies().First(movie => movie.Id == id));
		}

		public static void MockGetByName()
		{
			movieRepoMock.Setup(repo => repo.GetByName(It.IsAny<string>()))
				.Returns((string name) => ListAllMovies().First(movie => movie.Name == name));
		}

		public static void MockAdd()
		{
			movieRepoMock.Setup(repo => repo.Add(It.IsAny<Movie>(), It.IsAny<string>(), It.IsAny<string>())).Returns((Movie movie,string actorIds,string genreIds)=>
			{
				if (actorIds.Contains("10"))
					throw new Exception("Actor Id(s) are not present");
				if (genreIds.Contains("10"))
					throw new Exception("Genre Id(s) are not present");
				if(movie.ProducerId==10)
					throw new Exception("Producer Id is not present");
				return 1;
			});
		}

		public static void MockPartialUpdate()
		{
			movieRepoMock.Setup(repo => repo.PartialUpdate(It.IsAny<string>(), It.IsAny<DynamicParameters>())).Callback((string sql, DynamicParameters dbArgs) =>
			   {
				   if (dbArgs.ParameterNames.Contains("ProducerId") && dbArgs.Get<int>("ProducerId") == 10)
					   throw new Exception("dbo.Producers");
				   if (dbArgs.ParameterNames.Contains("ActorIds") && dbArgs.Get<string>("ActorIds").Contains("10"))
					   throw new Exception("dbo.Actors");
				   if (dbArgs.ParameterNames.Contains("GenreIds") && dbArgs.Get<string>("GenreIds").Contains("10"))
					   throw new Exception("dbo.Genres");
				   int count = new Regex("@Name", RegexOptions.Compiled | RegexOptions.IgnoreCase).Matches(sql).Count;
				   if (count > 1)
					   throw new Exception("Property is specified more than once");

			   });
		}

		public static void MockUpdate()
		{
			movieRepoMock.Setup(repo => repo.Update(It.IsAny<Movie>(), It.IsAny<string>(), It.IsAny<string>())).Callback((Movie movie,string actorIds,string genreIds)=>
			{
				if (movie.Id == 10)
					throw new Exception("dbo.Movies");
				if (actorIds.Contains("10"))
					throw new Exception("dbo.Actors");
				if (genreIds.Contains("10"))
					throw new Exception("dbo.Genres");
				if (movie.ProducerId == 10)
					throw new Exception("dbo.Producers");
			});
		}

		public static void MockRemove()
		{
			movieRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
		}

		private static IEnumerable<Movie> ListAllMovies()
		{
			var movies = new List<Movie>
			{
				new Movie
				{
					Id=1,
					Name="Terminal",
					YearOfRelease=2006,
					Plot="An english movie",
					PosterURL="URL",
					ProducerId=1
				}
			};
			return movies;
		}
	}
}
