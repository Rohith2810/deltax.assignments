﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Moq;

namespace IMDB.API.Tests.MockResources
{
	public class GenreMock
	{
		public static readonly Mock<IGenreRepository> genreRepoMock = new Mock<IGenreRepository>();

		public static void MockGetAll()
		{
			genreRepoMock.Setup(repo => repo.GetAll()).Returns(ListAllGenres());
		}

		public static void MockGetById()
		{
			genreRepoMock.Setup(repo => repo.GetById(It.IsAny<int>()))
				.Returns((int id) =>
				{
					if (id == 10)
						throw new Exception("Sequence contains no elements");
					return ListAllGenres().First(genre => genre.Id == id);
				});
		}

		public static void MockGetByMovieId()
		{
			genreRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>())).Returns((int movieId) =>
			new List<Genre>
			{
				new Genre
				{
					Id = 1,
					Name = "Action"
				},
				new Genre
				{
					Id = 2,
					Name = "Sci-Fi"
				}
			});
		}
		public static void MockAdd()
		{
			genreRepoMock.Setup(repo => repo.Add(It.IsAny<Genre>())).Returns(1);
		}

		public static void MockPartialUpdate()
		{
			genreRepoMock.Setup(repo => repo.PartialUpdate(It.IsAny<string>(), It.IsAny<DynamicParameters>())).Callback((string sql, DynamicParameters dbArgs) =>
			{
				int count = new Regex("@Name", RegexOptions.Compiled | RegexOptions.IgnoreCase).Matches(sql).Count;
				if (count > 1)
					throw new Exception("Property is specified more than once");
			});
		}

		public static void MockUpdate()
		{
			genreRepoMock.Setup(repo => repo.Update(It.IsAny<Genre>()));
		}

		public static void MockRemove()
		{
			genreRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
		}

		private static IEnumerable<Genre> ListAllGenres()
		{
			var genres = new List<Genre>
			{
				new Genre
				{
					Id = 1,
					Name = "Action"
				},
				new Genre
				{
					Id = 2,
					Name = "Sci-Fi"
				}
			};
			return genres;
		}
	}
}
