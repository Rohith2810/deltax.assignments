﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using IMDB.API.Models.Request;
using IMDB.API.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace IMDB.API.Controllers
{
	[Route("actors")]
	[ApiController]
	public class ActorsController : ControllerBase
	{
		private readonly IActorService _actorService;

		public ActorsController(IActorService actorService)
		{
			_actorService = actorService;
		}

		[HttpPost]
		public IActionResult Add([FromBody] ActorRequest actorRequest)
		{
			try
			{
				var id = _actorService.Add(actorRequest);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				return BadRequest(exception.Message);
			}
		}

		[HttpGet]
		public IActionResult GetAll()
		{
			var actors = _actorService.GetAll();
			return new JsonResult(actors);
		}

		[HttpGet("{id}")]
		public IActionResult GetById(int id)
		{
			try
			{
				var actor = _actorService.GetById(id);
				return new JsonResult(actor);
			}
			catch (Exception)
			{
				return NotFound($"No Actor is present with given id '{id}'");
			}
		}

		[HttpPut("{id}")]
		public IActionResult Update(int id, [FromBody] ActorRequest actorRequest)
		{
			try
			{
				_actorService.Update(id, actorRequest);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if(exception.Message=="Sequence contains no elements")
					return NotFound($"No Actor is present with given id '{id}'");
				return BadRequest(exception.Message);
			}
		}

		[HttpPatch("{id}")]
		public IActionResult PartialUpdate(int id,[FromBody] List<PatchDto> patchDtos)
		{
			try
			{
				_actorService.PartialUpdate(id, patchDtos);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if (exception.Message.Contains("Sequence contains no elements"))
					return NotFound($"No Actor is present with given id '{id}'");
				if (exception.Message.Contains("specified more than once"))
					return BadRequest("A property is given more than once");
				return BadRequest(exception.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Remove(int id)
		{
			try
			{
				_actorService.Remove(id);
				return Ok(new { Id = id });
			}
			catch (Exception)
			{
				return NotFound($"No Actor is present with given id '{id}'");
			}
		}
	}
}
