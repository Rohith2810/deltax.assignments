﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using IMDB.API.Models.Request;
using IMDB.API.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Firebase.Storage;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace IMDB.API.Controllers
{
	[Route("movies")]
	[ApiController]
	public class MoviesController : ControllerBase
	{
		private readonly IMovieService _movieService;

		public MoviesController(IMovieService movieService)
		{
			_movieService = movieService;
		}

		[HttpPost]
		public IActionResult Add([FromBody] MovieRequest movieRequest)
		{
			try
			{
				var id = _movieService.Add(movieRequest);
				return Ok(new { Id = id });
			}
			catch (SqlException exception)
			{
				if (exception.Message.Contains("dbo.Producers"))
					return BadRequest("Producer Id is not present");
				if (exception.Message.Contains("dbo.Actors"))
					return BadRequest("Actor Id(s) are not present");
				return BadRequest("Genre Id(s) are not present");
			}
			catch (Exception exception)
			{
				return BadRequest(exception.Message);
			}
		}

		[HttpPost("upload")]
		public async Task<IActionResult> UploadFile(IFormFile file)
		{
			if (file == null || file.Length == 0)
				return Content("file not selected");
			var task = await new FirebaseStorage("imdb-efbaa.appspot.com")
					.Child("Posters")
					.Child(Guid.NewGuid().ToString() + ".jpg")
					.PutAsync(file.OpenReadStream());
			return Ok(task);
		}


		[HttpGet]
		public IActionResult GetAll()
		{
			var movies = _movieService.GetAll();
			return new JsonResult(movies);
		}

		[HttpGet("{id}")]
		public IActionResult GetById(int id)
		{
			try
			{
				var movie = _movieService.GetById(id);
				return new JsonResult(movie);
			}
			catch (Exception)
			{
				return NotFound($"No Movie is present with given id '{id}'");
			}
		}

		[HttpGet("[action]")]
		public IActionResult GetByName(string name)
		{
			try
			{
				var movie = _movieService.GetByName(name);
				return new JsonResult(movie);
			}
			catch (Exception)
			{
				return NotFound($"No Movie is present with given name '{name}'");
			}
		}

		[HttpPatch("{id}")]
		public IActionResult PartialUpdate(int id, [FromBody] List<PatchDto> patchDtos)
		{
			try
			{
				_movieService.PartialUpdate(id, patchDtos);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if (exception.Message.Contains("Sequence contains no"))
					return NotFound($"No Movie is present with given id '{id}'");
				if (exception.Message.Contains("specified more than once"))
					return BadRequest("A property is given more than once");
				if (exception.Message.Contains("dbo.Producers"))
					return BadRequest("Producer Id is not present");
				if (exception.Message.Contains("dbo.Actors"))
					return BadRequest("Actor Id(s) are not present");
				if (exception.Message.Contains("dbo.Genres"))
					return BadRequest("Genre Id(s) are not present");
				return BadRequest(exception.Message);
			}
		}

		[HttpPut("{id}")]
		public IActionResult Update(int id, [FromBody] MovieRequest movieRequest)
		{
			try
			{
				_movieService.Update(id, movieRequest);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if (exception.Message.Contains("dbo.Movies"))
					return NotFound($"Movie is not present with given id '{id}'");
				if (exception.Message.Contains("dbo.Producers"))
					return BadRequest("Producer Id is not present");
				if (exception.Message.Contains("dbo.Actors"))
					return BadRequest("Actor Id(s) are not present");
				if(exception.Message.Contains("dbo.Genres"))
					return BadRequest("Genre Id(s) are not present");
				return BadRequest(exception.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Remove(int id)
		{
			try
			{
				_movieService.Remove(id);
				return Ok(new { Id = id });
			}
			catch (Exception)
			{
				return NotFound($"No Movie is present with given id '{id}'");
			}
		}
	}
}
