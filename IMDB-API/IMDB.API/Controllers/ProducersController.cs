﻿using System;
using System.Collections.Generic;
using IMDB.API.Models.Request;
using IMDB.API.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace IMDB.API.Controllers
{
	[Route("producers")]
	[ApiController]
	public class ProducersController : ControllerBase
	{
		private readonly IProducerService _producerService;

		public ProducersController(IProducerService producerService)
		{
			_producerService = producerService;
		}

		[HttpPost]
		public IActionResult Add(ProducerRequest producerRequest)
		{
			try
			{
				var id = _producerService.Add(producerRequest);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				return BadRequest(exception.Message);
			}
		}

		[HttpGet]
		public IActionResult GetAll()
		{
			var producers = _producerService.GetAll();
			return new JsonResult(producers);
		}

		[HttpGet("{id}")]
		public IActionResult GetById(int id)
		{
			try
			{
				var producer = _producerService.GetById(id);
				return new JsonResult(producer);
			}
			catch (Exception)
			{
				return NotFound($"No Producer is present with given id '{id}'");
			}
			
		}

		[HttpPatch("{id}")]
		public IActionResult PartialUpdate(int id, [FromBody] List<PatchDto> patchDtos)
		{
			try
			{
				_producerService.PartialUpdate(id, patchDtos);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if (exception.Message.Contains("Sequence contains no elements"))
					return NotFound($"No Producer is present with given id '{id}'");
				if (exception.Message.Contains("specified more than once"))
					return BadRequest("A property is given more than once");
				return BadRequest(exception.Message);
			}
		}

		[HttpPut("{id}")]
		public IActionResult Update(int id, ProducerRequest producerRequest)
		{
			try
			{
				_producerService.Update(id, producerRequest);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if (exception.Message == "Sequence contains no elements")
					return NotFound($"No Producer is present with given id '{id}'");
				return BadRequest(exception.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Remove(int id)
		{
			try
			{
				_producerService.Remove(id);
				return Ok(new { Id = id });
			}
			catch (Exception)
			{
				return NotFound($"No Producer is present with given id '{id}'");
			}
		}
	}
}
