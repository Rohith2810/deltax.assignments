﻿using System;
using System.Collections.Generic;
using IMDB.API.Models.Request;
using IMDB.API.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace IMDB.API.Controllers
{
	[Route("genres")]
	[ApiController]
	public class GenresController : ControllerBase
	{
		private readonly IGenreService _genreService;

		public GenresController(IGenreService genreService)
		{
			_genreService = genreService;
		}

		[HttpPost]
		public IActionResult Add([FromBody] GenreRequest genreRequest)
		{
			try
			{
				var id = _genreService.Add(genreRequest);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				return BadRequest(exception.Message);
			}
		}

		[HttpGet]
		public IActionResult GetAll()
		{
			var genres = _genreService.GetAll();
			return new JsonResult(genres);
		}

		[HttpGet("{id}")]
		public IActionResult GetById(int id)
		{
			try
			{
				var genre = _genreService.GetById(id);
				return new JsonResult(genre);
			}
			catch (Exception)
			{
				return NotFound($"No Genre is present with given id '{id}'");
			}
		}

		[HttpPatch("{id}")]
		public IActionResult PartialUpdate(int id, [FromBody] List<PatchDto> patchDtos)
		{
			try
			{
				_genreService.PartialUpdate(id, patchDtos);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if (exception.Message.Contains("Sequence contains no"))
					return NotFound($"No Actor is present with given id '{id}'");
				if (exception.Message.Contains("specified more than once"))
					return BadRequest("A property is given more than once");
				return BadRequest(exception.Message);
			}
		}

		[HttpPut("{id}")]
		public IActionResult Update(int id, [FromBody] GenreRequest genreRequest)
		{
			try
			{
				_genreService.Update(id, genreRequest);
				return Ok(new { Id = id });
			}
			catch (Exception exception)
			{
				if (exception.Message == "Sequence contains no elements")
					return NotFound($"No Genre is present with given id '{id}'");
				return BadRequest(exception.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Remove(int id)
		{
			try
			{
				_genreService.Remove(id);
				return Ok(new { Id = id });
			}
			catch (Exception)
			{
				return NotFound($"No Genre is present with given id '{id}'");
			}
		}
	}
}
