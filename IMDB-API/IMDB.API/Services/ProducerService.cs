﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Models.Request;
using IMDB.API.Models.Response;
using IMDB.API.Repositories.Interfaces;
using IMDB.API.Services.Interfaces;

namespace IMDB.API.Services
{
	public class ProducerService : IProducerService
	{
		private readonly IProducerRepository _producerRepository;

		public ProducerService(IProducerRepository producerRepository)
		{
			_producerRepository = producerRepository;
		}

		public void Validate(ProducerRequest producer)
		{
			if (string.IsNullOrEmpty(producer.Name))
				throw new ArgumentException("Producer name can't be null or empty");
			if (string.IsNullOrEmpty(producer.Bio))
				throw new ArgumentException("Producer bio can't be null or empty");
			if (string.IsNullOrEmpty(producer.Sex))
				throw new ArgumentException("Producer sex can't be null or empty");
			if (!producer.Sex.Equals("male", StringComparison.CurrentCultureIgnoreCase) && !producer.Sex.Equals("female", StringComparison.CurrentCultureIgnoreCase))
				throw new ArgumentException("Producer sex is invalid");
			if (producer.DOB.CompareTo(DateTime.Today) > 0)
				throw new ArgumentException("Producer date of birth should be before current date");
		}

		public int Add(ProducerRequest producerRequest)
		{
			Validate(producerRequest);
			return _producerRepository.Add(new Person
			{
				Name = producerRequest.Name,
				Bio = producerRequest.Bio,
				DOB = producerRequest.DOB,
				Sex = producerRequest.Sex
			});
		}

		public IEnumerable<ProducerResponse> GetAll()
		{
			return _producerRepository.GetAll().Select(producer => new ProducerResponse
			{
				Id = producer.Id,
				Name = producer.Name,
				Bio = producer.Bio,
				DOB = producer.DOB,
				Sex = producer.Sex
			});
		}

		public ProducerResponse GetById(int id)
		{
			var producer = _producerRepository.GetById(id);
			return new ProducerResponse
			{
				Id = producer.Id,
				Name = producer.Name,
				Bio = producer.Bio,
				DOB = producer.DOB,
				Sex = producer.Sex
			};
		}

		public void PartialUpdate(int id, List<PatchDto> patchDtos)
		{
			_producerRepository.GetById(id);
			var sql = @"Update Producers SET ";
			var dbArgs = new DynamicParameters();
			var properties = new List<string>();
			foreach (var patchDto in patchDtos)
			{
				if (string.IsNullOrEmpty(patchDto.PropertyName))
					throw new ArgumentException("Property name can't be null or empty");
				if (string.IsNullOrEmpty(patchDto.PropertyValue))
					throw new ArgumentException("Property value can't be null or empty");
				switch (patchDto.PropertyName)
				{
					case "Name":
						properties.Add("Name=@Name");
						dbArgs.Add("@Name", patchDto.PropertyValue);
						break;
					case "Bio":
						properties.Add("Bio=@Bio");
						dbArgs.Add("@Bio", patchDto.PropertyValue);
						break;
					case "DOB":
						DateTime dob;
						if (!DateTime.TryParse(patchDto.PropertyValue, out dob))
							throw new ArgumentException("Producer date of birth should be a valid date");
						if (dob.CompareTo(DateTime.Today) > 0)
							throw new ArgumentException("Producer date of birth should be before current date");
						properties.Add("DOB=@DOB");
						dbArgs.Add("@DOB", patchDto.PropertyValue);
						break;
					case "Sex":
						if (!patchDto.PropertyValue.Equals("male", StringComparison.CurrentCultureIgnoreCase) && !patchDto.PropertyValue.Equals("female", StringComparison.CurrentCultureIgnoreCase))
							throw new ArgumentException("Producer sex is invalid");
						properties.Add("Sex=@Sex");
						dbArgs.Add("@Sex", patchDto.PropertyValue);
						break;
					default:
						throw new Exception($"Invalid property name '{patchDto.PropertyName}'");
				}
			}
			dbArgs.Add("@Id", id);
			sql += string.Join(',', properties) + " WHERE Id=@Id";
			_producerRepository.PartialUpdate(sql, dbArgs);
		}

		public void Remove(int id)
		{
			_producerRepository.GetById(id);
			_producerRepository.Remove(id);
		}

		public void Update(int id, ProducerRequest producerRequest)
		{
			Validate(producerRequest);
			_producerRepository.GetById(id);
			_producerRepository.Update(new Person
			{
				Id=id,
				Name = producerRequest.Name,
				Bio = producerRequest.Bio,
				DOB = producerRequest.DOB,
				Sex = producerRequest.Sex
			});
		}
	}
}
