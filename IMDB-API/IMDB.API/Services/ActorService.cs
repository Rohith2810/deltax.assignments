﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Models.Request;
using IMDB.API.Models.Response;
using IMDB.API.Repositories.Interfaces;
using IMDB.API.Services.Interfaces;

namespace IMDB.API.Services
{
	public class ActorService : IActorService
	{
		private readonly IActorRepository _actorRepository;

		public ActorService(IActorRepository actorRepository)
		{
			_actorRepository = actorRepository;
		}

		public void Validate(ActorRequest actor)
		{
			if (string.IsNullOrEmpty(actor.Name))
				throw new ArgumentException("Actor name can't be null or empty");
			if (string.IsNullOrEmpty(actor.Bio))
				throw new ArgumentException("Actor bio can't be null or empty");
			if(string.IsNullOrEmpty(actor.Sex))
				throw new ArgumentException("Actor sex can't be null or empty");
			if (!actor.Sex.Equals("male", StringComparison.CurrentCultureIgnoreCase) && !actor.Sex.Equals("female", StringComparison.CurrentCultureIgnoreCase))
				throw new ArgumentException("Actor sex is invalid");
			if (actor.DOB.CompareTo(DateTime.Today) > 0)
				throw new ArgumentException("Actor date of birth should be before current date");
		}

		public int Add(ActorRequest actorRequest)
		{
			Validate(actorRequest);
			return _actorRepository.Add(new Person
			{
				Name = actorRequest.Name,
				DOB = actorRequest.DOB,
				Sex = actorRequest.Sex,
				Bio = actorRequest.Bio
			});
		}

		public IEnumerable<ActorResponse> GetAll()
		{
			return _actorRepository.GetAll().Select(actor => new ActorResponse
			{
				Id = actor.Id,
				Name = actor.Name,
				Sex = actor.Sex,
				DOB = actor.DOB,
				Bio = actor.Bio
			});
		}

		public ActorResponse GetById(int id)
		{
			var actor = _actorRepository.GetById(id);
			return new ActorResponse
			{
				Id = actor.Id,
				Name = actor.Name,
				Sex = actor.Sex,
				DOB = actor.DOB,
				Bio = actor.Bio
			};
		}

		public IEnumerable<ActorResponse> GetByMovieId(int id)
		{
			return _actorRepository.GetByMovieId(id).Select(actor => new ActorResponse
			{
				Id = actor.Id,
				Name = actor.Name,
				Sex = actor.Sex,
				DOB = actor.DOB,
				Bio = actor.Bio
			});
		}

		public void PartialUpdate(int id, List<PatchDto> patchDtos)
		{
			_actorRepository.GetById(id);
			var sql = @"Update Actors SET ";
			var dbArgs = new DynamicParameters();
			var properties = new List<string>();
			foreach (var patchDto in patchDtos)
			{
				if (string.IsNullOrEmpty(patchDto.PropertyName))
					throw new ArgumentException("Property name can't be null or empty");
				if (string.IsNullOrEmpty(patchDto.PropertyValue))
					throw new ArgumentException("Property value can't be null or empty");
				switch (patchDto.PropertyName)
				{
					case "Name":
						properties.Add("Name=@Name");
						dbArgs.Add("@Name", patchDto.PropertyValue);
						break;
					case "Bio":
						properties.Add("Bio=@Bio");
						dbArgs.Add("@Bio", patchDto.PropertyValue);
						break;
					case "DOB":
						DateTime dob;
						if (!DateTime.TryParse(patchDto.PropertyValue, out dob))
							throw new ArgumentException("Actor date of birth should be a valid date");
						if (dob.CompareTo(DateTime.Today) > 0)
							throw new ArgumentException("Actor date of birth should be before current date");
						properties.Add("DOB=@DOB");
						dbArgs.Add("@DOB", patchDto.PropertyValue);
						break;
					case "Sex":
						if (!patchDto.PropertyValue.Equals("male", StringComparison.CurrentCultureIgnoreCase) && !patchDto.PropertyValue.Equals("female", StringComparison.CurrentCultureIgnoreCase))
							throw new ArgumentException("Actor sex is invalid");
						properties.Add("Sex=@Sex");
						dbArgs.Add("@Sex", patchDto.PropertyValue);
						break;
					default:
						throw new Exception($"Invalid property name '{patchDto.PropertyName}'");
				}
			}
			dbArgs.Add("@Id", id);
			sql += string.Join(',', properties)+" WHERE Id=@Id";
			_actorRepository.PartialUpdate(sql, dbArgs);
		}

		public void Remove(int id)
		{
			_actorRepository.GetById(id);
			_actorRepository.Remove(id);
		}

		public void Update(int id, ActorRequest actorRequest)
		{
			Validate(actorRequest);
			_actorRepository.GetById(id);
			_actorRepository.Update(new Person
			{
				Id=id,
				Name = actorRequest.Name,
				DOB = actorRequest.DOB,
				Sex = actorRequest.Sex,
				Bio = actorRequest.Bio
			});
		}
	}
}
