﻿using System.Collections.Generic;
using IMDB.API.Models.Request;
using IMDB.API.Models.Response;

namespace IMDB.API.Services.Interfaces
{
	public interface IMovieService
	{
		public int Add(MovieRequest movieRequest);
		public IEnumerable<MovieResponse> GetAll();
		public MovieResponse GetById(int id);
		public MovieResponse GetByName(string name);
		public void PartialUpdate(int id, List<PatchDto> patchDtos);
		public void Update(int id, MovieRequest movieRequest);
		public void Remove(int id);
	}
}
