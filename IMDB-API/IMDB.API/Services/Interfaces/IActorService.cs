﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDB.API.Models.Request;
using IMDB.API.Models.Response;

namespace IMDB.API.Services.Interfaces
{
	public interface IActorService
	{
		public int Add(ActorRequest actorRequest);
		public IEnumerable<ActorResponse> GetAll();
		public ActorResponse GetById(int id);
		public void PartialUpdate(int id, List<PatchDto> patchDtos); 
		public void Update(int id, ActorRequest actorRequest);
		public void Remove(int id);
		public IEnumerable<ActorResponse> GetByMovieId(int id);
	}
}
