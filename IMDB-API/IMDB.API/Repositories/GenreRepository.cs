﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Microsoft.Extensions.Options;

namespace IMDB.API.Repositories
{
	public class GenreRepository : BaseRepository<Genre>, IGenreRepository
	{
		private readonly ConnectionString _connectionString;

		public GenreRepository(IOptions<ConnectionString> connectionString)
			:base(connectionString.Value)
		{
			_connectionString = connectionString.Value;
		}
		public int Add(Genre genre)
		{
			const string sql = @"
			INSERT INTO Genres(
			Name
			)
			VALUES(
			@Name
			);
			SELECT CAST(SCOPE_IDENTITY() as int)";
			return BaseAdd(sql, genre);
		}

		public IEnumerable<Genre> GetAll()
		{
			const string sql = @"
			SELECT *
			FROM Genres";
			return BaseGetAll(sql);
		}

		public Genre GetById(int id)
		{
			const string sql = @"
			SELECT *
			FROM Genres
			WHERE Id=@Id"; 
			return BaseGetById(sql, id);
		}

		public IEnumerable<Genre> GetByMovieId(int movieId)
		{
			const string sql = @"
			SELECT G.Id AS Id,
			G.Name AS Name
			FROM MovieGenresMapping MGM
			INNER JOIN Genres G
			ON MGM.GenreId=G.Id
			WHERE MGM.MovieId=@MovieId";
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				return connection.Query<Genre>(sql, new
				{
					MovieId = movieId
				});
			}
		}

		public void PartialUpdate(string sql, DynamicParameters dbArgs)
		{
			BasePartialUpdate(sql, dbArgs);
		}

		public void Remove(int id)
		{
			const string sql = @"
			EXEC [usp_DeleteGenre]
			@Id";
			BaseRemove(sql, id);
		}

		public void Update(Genre genre)
		{
			const string sql = @"
			UPDATE Genres
			SET
			Name=@Name
			Where Id=@Id";
			BaseUpdate(sql, genre);
		}
	}
}
