﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;

namespace IMDB.API.Repositories
{
	public class BaseRepository<T>
	{
		private readonly ConnectionString _connectionString;

		public BaseRepository(ConnectionString connectionString)
		{
			_connectionString = connectionString;
		}

		public int BaseAdd(string sql, T entity)
		{
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				var id = connection.QueryFirst<int>(sql, entity);
				return id;
			}
		}

		public IEnumerable<T> BaseGetAll(string sql)
		{
			using var connection = new SqlConnection(_connectionString.DB);
			return connection.Query<T>(sql);
		}
		public T BaseGetById(string sql, int id)
		{
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				return connection.QueryFirst<T>(sql, new
				{
					Id = id
				});
			}	
		}

		public void BasePartialUpdate(string sql, DynamicParameters dbArgs)
		{
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				connection.Execute(sql, dbArgs);
			}
		}

		public void BaseRemove(string sql, int id)
		{
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				connection.Execute(sql, new
				{
					Id = id
				});
			}
		}
		public void BaseUpdate(string sql, T entity)
		{
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				connection.Execute(sql, entity);
			}
		}
	}
}
