﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;

namespace IMDB.API.Repositories.Interfaces
{
	public interface IMovieRepository
	{
		public int Add(Movie movie, string actorIds, string genreIds);
		public IEnumerable<Movie> GetAll();
		public Movie GetById(int id);
		public Movie GetByName(string name);
		public void PartialUpdate(string sql, DynamicParameters dbArgs);
		public void Update(Movie movie, string actroIds, string genreIds);
		public void Remove(int id);
	}
}
