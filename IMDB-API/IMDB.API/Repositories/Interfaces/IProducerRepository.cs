﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;

namespace IMDB.API.Repositories.Interfaces
{
	public interface IProducerRepository
	{
		public int Add(Person producer);
		public IEnumerable<Person> GetAll();
		public Person GetById(int id);
		public void PartialUpdate(string sql, DynamicParameters dbArgs);
		public void Update(Person producer);
		public void Remove(int id);
	}
}
