﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;

namespace IMDB.API.Repositories.Interfaces
{
	public interface IGenreRepository
	{
		public int Add(Genre genre);
		public IEnumerable<Genre> GetAll();
		public Genre GetById(int id);
		public void PartialUpdate(string sql, DynamicParameters dbArgs);
		public void Update(Genre genre);
		public void Remove(int id);
		public IEnumerable<Genre> GetByMovieId(int id);
	}
}
