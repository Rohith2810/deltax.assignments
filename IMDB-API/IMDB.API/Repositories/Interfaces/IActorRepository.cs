﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using IMDB.API.Models.DB;

namespace IMDB.API.Repositories.Interfaces
{
	public interface IActorRepository
	{
		public int Add(Person actor);
		public IEnumerable<Person> GetAll();
		public Person GetById(int id);
		public void PartialUpdate(string sql,DynamicParameters dbArgs);
		public void Update(Person actor);
		public void Remove(int id);
		public IEnumerable<Person> GetByMovieId(int id);
	}
}
