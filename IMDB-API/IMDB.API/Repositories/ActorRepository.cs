﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Microsoft.Extensions.Options;

namespace IMDB.API.Repositories
{
	public class ActorRepository : BaseRepository<Person>,IActorRepository
	{
		private readonly ConnectionString _connectionString;

		public ActorRepository(IOptions<ConnectionString> connectionString)
			:base(connectionString.Value)
		{
			_connectionString = connectionString.Value;
		}
		public int Add(Person actor)
		{
			const string sql = @"
			INSERT INTO Actors(
			Name,
			Sex,
			DOB,
			Bio
			)
			VALUES(
			@Name,
			@Sex,
			@DOB,
			@Bio
			);
			SELECT CAST(SCOPE_IDENTITY() as int);";
			return BaseAdd(sql, actor);
		}

		public IEnumerable<Person> GetAll()
		{
			const string sql = @"
			SELECT *
			FROM Actors";
			
			return BaseGetAll(sql);
		}

		public Person GetById(int id)
		{
			const string sql = @"
			SELECT *
			FROM Actors
			WHERE Id=@Id";
			return BaseGetById(sql, id);
		}

		public IEnumerable<Person> GetByMovieId(int movieId)
		{
			const string sql = @"
			SELECT A.Id as Id,
			A.Name as Name,
			A.Bio as Bio,
			A.DOB as DOB,
			A.Sex as Sex
			FROM MovieActorsMapping MAM
			INNER JOIN Actors A
			ON MAM.ActorId=A.Id
			WHERE MAM.MovieId=@MovieId";
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				return connection.Query<Person>(sql, new
				{
					MovieId = movieId
				});
			}
		}

		public void PartialUpdate(string sql, DynamicParameters dbArgs)
		{
			BasePartialUpdate(sql, dbArgs);
		}

		public void Remove(int id)
		{
			const string sql = @"
			EXEC [usp_DeleteActor]
			@Id";
			BaseRemove(sql, id);
		}

		public void Update(Person actor)
		{
			const string sql = @"
			UPDATE Actors
			SET 
			Name=@Name,
			Sex=@Sex,
			DOB=@DOB,
			Bio=@Bio
			WHERE Id=@Id";
			BaseUpdate(sql, actor);
		}
	}
}
