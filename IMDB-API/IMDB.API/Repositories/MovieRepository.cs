﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Microsoft.Extensions.Options;

namespace IMDB.API.Repositories
{
	public class MovieRepository : BaseRepository<Movie>,IMovieRepository
	{
		private readonly ConnectionString _connectionString;

		public MovieRepository(IOptions<ConnectionString> connectionString)
			:base(connectionString.Value)
		{
			_connectionString = connectionString.Value;
		}
		public int Add(Movie movie,string actorIds,string genreIds)
		{
			const string sql = @"
			EXEC [usp_AddMovie]
			@Name,
			@YearOfRelease,
			@ProducerId,
			@Plot,
			@PosterURL,
			@ActorIds,
			@GenreIds";
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				var id = connection.QueryFirst<int>(sql, new
				{
					movie.Name,
					movie.YearOfRelease,
					movie.ProducerId,
					movie.Plot,
					movie.PosterURL,
					ActorIds = actorIds,
					GenreIds = genreIds
				});
				return id;
			}
		}

		public IEnumerable<Movie> GetAll()
		{
			const string sql = @"
			SELECT *
			FROM Movies";
			return BaseGetAll(sql);
		}

		public Movie GetById(int id)
		{
			const string sql = @"
			SELECT *
			FROM Movies
			WHERE Id=@Id";
			return BaseGetById(sql, id);
		}

		public Movie GetByName(string name)
		{
			const string sql = @"
			SELECT *
			FROM Movies
			WHERE Name=@Name";
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				var movie = connection.QueryFirst<Movie>(sql, new
				{
					Name = name
				});
				return movie;
			}
		}

		public void PartialUpdate(string sql, DynamicParameters dbArgs)
		{
			BasePartialUpdate(sql, dbArgs);
		}

		public void Remove(int id)
		{
			const string sql = @"
			EXEC [usp_DeleteMovie]
			@Id";
			BaseRemove(sql, id);
		}

		public void Update(Movie movie,string actorIds,string genreIds)
		{
			const string sql = @"
			EXEC [usp_UpdateMovie]
			@Id,
			@Name,
			@YearOfRelease,
			@ProducerId,
			@Plot,
			@PosterURL,
			@ActorIds,
			@GenreIds";
			using (var connection = new SqlConnection(_connectionString.DB))
			{
				connection.Query(sql, new
				{
					movie.Id,
					movie.Name,
					movie.YearOfRelease,
					movie.ProducerId,
					movie.Plot,
					movie.PosterURL,
					ActorIds = actorIds,
					GenreIds = genreIds
				});
			}
		}
	}
}
