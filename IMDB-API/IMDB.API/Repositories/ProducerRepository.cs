﻿using System.Collections.Generic;
using Dapper;
using IMDB.API.Models.DB;
using IMDB.API.Repositories.Interfaces;
using Microsoft.Extensions.Options;

namespace IMDB.API.Repositories
{
	public class ProducerRepository : BaseRepository<Person>,IProducerRepository
	{

		public ProducerRepository(IOptions<ConnectionString> connectionString)
			:base(connectionString.Value)
		{
		}
		public int Add(Person producer)
		{
			const string sql = @"
			INSERT INTO Producers(
			Name,
			Bio,
			DOB,
			Sex
			)
			VALUES(
			@Name,
			@Bio,
			@DOB,
			@Sex
			);
			SELECT CAST(SCOPE_IDENTITY() as int)";
			return BaseAdd(sql, producer);
		}

		public IEnumerable<Person> GetAll()
		{
			const string sql = @"
			SELECT *
			FROM Producers";
			return BaseGetAll(sql);
		}

		public Person GetById(int id)
		{
			const string sql = @"
			SELECT *
			FROM Producers
			WHERE Id=@Id";
			return BaseGetById(sql, id);
		}

		public void PartialUpdate(string sql, DynamicParameters dbArgs)
		{
			BasePartialUpdate(sql, dbArgs);
		}

		public void Remove(int id)
		{
			const string sql = @"
			DELETE
			FROM Producers
			WHERE Id=@Id";
			BaseRemove(sql, id);
		}

		public void Update(Person producer)
		{
			const string sql = @"
			UPDATE Producers
			SET
			Name=@Name,
			Bio=@Bio,
			DOB=@DOB,
			Sex=@Sex
			WHERE Id=@Id";
			BaseUpdate(sql, producer);
		}
	}
}
