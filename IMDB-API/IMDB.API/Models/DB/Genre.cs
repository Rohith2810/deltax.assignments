﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.API.Models.DB
{
	public class Genre
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
