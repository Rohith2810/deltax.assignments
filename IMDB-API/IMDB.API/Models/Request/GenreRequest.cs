﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.API.Models.Request
{
	public class GenreRequest
	{
		public string Name { get; set; }
	}
}
