﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.API.Models.Response
{
	public class GenreResponse
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
