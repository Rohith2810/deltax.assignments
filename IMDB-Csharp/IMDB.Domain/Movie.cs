﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Domain
{
	public class Movie
	{
		public string Name { get; private set; }
		public int YearOfRelease { get; private set; }
		public string Plot { get; private set; }
		public Person Producer { get; private set; }
		public List<Person> Actors { get; private set; }
		
		public Movie(string name,int yearOfRelease,string plot, Person producer,List<Person> actors)
		{
			Name = name;
			YearOfRelease = yearOfRelease;
			Plot = plot;
			Producer= producer;
			Actors = actors.ToList(); ;
		}
	}
}
