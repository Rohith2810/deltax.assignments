﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDB.Domain
{
	public class Person
	{
		public string Name { get; private set; }
		public DateTime DateOfBirth { get; private set; }
		
		public Person(string name,DateTime dateOfBirth)
		{
			Name = name;
			DateOfBirth = dateOfBirth;
		}
	}
}
