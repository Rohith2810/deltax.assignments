﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;

namespace IMDB.Repository
{
	public class ProducerRepository
	{
		private readonly List<Person> _producers;

		public ProducerRepository()
		{
			_producers = new List<Person>();
		}

		public void Add(Person producer)
		{
			_producers.Add(producer);
		}

		public List<Person> Get()
		{
			return _producers.ToList();
		}
	}
}
