﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMDB.Domain;

namespace IMDB.Repository
{
	public class MovieRepository
	{
		private readonly List<Movie> _movies;

		public MovieRepository()
		{
			_movies = new List<Movie>();
		}

		public void Add(Movie movie)
		{
			_movies.Add(movie);
		}

		public List<Movie> Get()
		{
			return _movies.ToList();
		}

		public void Delete(Movie movie)
		{
			_movies.Remove(movie);
		}
	}
}
