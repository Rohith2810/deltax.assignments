﻿using System;
using System.Collections.Generic;
using IMDB.Domain;

namespace IMDB
{
	class Program
	{
		static void Main(string[] args)
		{
			var imdbService = new ImdbService();
			while (true)
			{
				Console.WriteLine("1. List Movies\n2. Add Movie\n3. Add Actor\n4. Add Producer\n5. Delete Movie\n6. Exit");
				Console.WriteLine("What do you want to do?");
				int choice = 0;
				try
				{
					choice = Convert.ToInt32(Console.ReadLine());
				}
				catch (Exception)
				{
					Console.WriteLine("Choice should be an integer in range(1-6)");
				}
				switch (choice)
				{
					case 1:
						var movies = imdbService.GetMovies();
						if (movies.Count == 0)
						{
							Console.WriteLine("There are no movies in IMDB");
							break;
						}
						Console.WriteLine("List of Movies");
						for (var i = 0; i < movies.Count; i++)
						{
							Console.WriteLine("Movie name: "+ movies[i].Name+"({0})",movies[i].YearOfRelease);
							Console.WriteLine("Plot: "+movies[i].Plot);
							var actorList = new string[movies[i].Actors.Count];
							var j = 0;
							foreach (var actor in movies[i].Actors)
							{
								actorList[j] = actor.Name;
								j++;
							}
							Console.WriteLine("Actors: "+string.Join(',',actorList));
							Console.WriteLine("Producer: "+movies[i].Producer.Name);
						}
						break;
					case 2:
						var producers = imdbService.GetPersons(ImdbService.PersonType.producer);
						var actors = imdbService.GetPersons(ImdbService.PersonType.actor);
						if (producers.Count == 0)
						{
							Console.WriteLine("There are no producers, first add producers");
							break;
						}
						if (actors.Count == 0)
						{
							Console.WriteLine("There are no actors, first add actors");
							break;
						}
						Console.WriteLine("Movie name:");
						var movieName = Console.ReadLine();
						Console.WriteLine("Year of release:");
						int yearOfRelease=0;
						try
						{
							yearOfRelease = Convert.ToInt32(Console.ReadLine());
						}
						catch (Exception)
						{
							Console.WriteLine("Year of release must be an integer");
							break;
						}
						Console.WriteLine("Plot:");
						var plot = Console.ReadLine();
						Console.WriteLine("Choose Producer:");
						for (var i = 0; i < producers.Count; i++)
						{
							Console.Write("{0}. {1} ", i + 1, producers[i].Name);
						}
						Console.WriteLine();
						int producerChoice=0;
						try
						{
							producerChoice = Convert.ToInt32(Console.ReadLine());
						}
						catch (Exception)
						{
							Console.WriteLine("Producer choice should be an integer");
							break;
						}
						Console.WriteLine("Choose Actors:");
						for (var i = 0; i < actors.Count; i++)
						{
							Console.Write("{0}. {1} ", i + 1, actors[i].Name);
						}
						Console.WriteLine();
						var actorChoices = Console.ReadLine().Split(' ');
						try
						{
							imdbService.AddMovie(movieName, yearOfRelease, plot, producerChoice, actorChoices);
							Console.WriteLine("Movie Added Succesfully");
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message);
						}
						break;
					case 3:
						Console.WriteLine("Actor name:");
						var actorName = Console.ReadLine();
						Console.WriteLine("Actor date of birth(yyyy/mm/dd):");
						var actorDob = Console.ReadLine();
						try
						{
							imdbService.AddPerson(actorName, actorDob, ImdbService.PersonType.actor);
							Console.WriteLine("Actor added succesfully");
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message);
						}
						break;
					case 4:
						Console.WriteLine("Producer name:");
						var producerName = Console.ReadLine();
						Console.WriteLine("Producer date of birth(yyyy/mm/dd):");
						var producerDob = Console.ReadLine();
						try
						{
							imdbService.AddPerson(producerName, producerDob, ImdbService.PersonType.producer);
							Console.WriteLine("Producer added succesfully");
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message);
						}
						break;
					case 5:
						var moviesPresent = imdbService.GetMovies();
						if (moviesPresent.Count == 0)
						{
							Console.WriteLine("There are no movies to delete");
							break;
						}
						Console.WriteLine("Choose Movie to delete");
						for (var i = 0; i < moviesPresent.Count; i++)
						{
							Console.Write("{0}. {1}({2}) ", i + 1, moviesPresent[i].Name,moviesPresent[i].YearOfRelease);
						}
						Console.WriteLine();
						int movieChoice=0;
						try
						{
							movieChoice = Convert.ToInt32(Console.ReadLine());
						}
						catch (Exception)
						{
							Console.WriteLine("Movie choice should be an integer");
							break;
						}
						try
						{
							imdbService.DeleteMovie(movieChoice);
							Console.WriteLine("Movie deleted succesfully");
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message);
						}
						break;
					case 6:
							return;
					default:
						Console.WriteLine("Enter a valid choice! Choice should be in range(1-6)");
						break;
				}
			}
		}
	}
}
