﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDB.Domain;
using IMDB.Repository;

namespace IMDB
{
	public class ImdbService
	{
		private ActorRepository _actorRepository;
		private ProducerRepository _producerRepository;
		private MovieRepository _movieRepository;

		public enum PersonType
		{
			actor,
			producer
		};
		public ImdbService()
		{
			_actorRepository = new ActorRepository();
			_producerRepository = new ProducerRepository();
			_movieRepository = new MovieRepository();
		}

		public void AddPerson(string name, string dateOfBirth, PersonType personType)
		{
			if (string.IsNullOrEmpty(name.Trim()) || string.IsNullOrEmpty(dateOfBirth.Trim()))
			{
				throw new ArgumentNullException("Actor name or date of birth can't be null or white space");
			}

			DateTime date;
			if (!DateTime.TryParse(dateOfBirth, out date))
			{
				throw new ArgumentException("Actor date of birth should be valid date");
			}

			if (date.CompareTo(DateTime.Today) > 0)
			{
				throw new ArgumentException("Actor date of birth should be before current date");
			}

			var person = new Person(name, date);
			if (personType == PersonType.actor)
			{
				_actorRepository.Add(person);
			}
			else
			{
				_producerRepository.Add(person);
			}
		}

		public List<Person> GetPersons(PersonType personType)
		{
			return personType == PersonType.actor ? _actorRepository.Get() : _producerRepository.Get();
		}

		public void AddMovie(string name, int yearOfRelease, string plot, int producerChoice, string[] actorsChoices)
		{
			var producersPresent = _producerRepository.Get();
			var actorsPresent = _actorRepository.Get();
			if (string.IsNullOrEmpty(name.Trim()) || yearOfRelease == 0 || string.IsNullOrEmpty(plot) || actorsChoices == null || actorsChoices.Length == 0)
			{
				throw new ArgumentNullException("Movie Attributes can't be null");
			}
			if (producerChoice < 1 || producerChoice > producersPresent.Count)
			{
				throw new ArgumentException("Producer choice is not valid.");
			}
			var producer = producersPresent[producerChoice - 1];
			var actors = new List<Person>();
			foreach (var actorChoice in actorsChoices)
			{
				int choice = 0;
				try
				{
					choice = Convert.ToInt32(actorChoice);
				}
				catch (Exception)
				{
					Console.WriteLine("Actor choice should be an integer");
				}
				if (choice < 1 || choice > actorsPresent.Count)
				{
					throw new ArgumentException("Actor choice is not valid");
				}
				actors.Add(actorsPresent[choice - 1]);
			}
			var movie = new Movie(name, yearOfRelease, plot, producer, actors);
			_movieRepository.Add(movie);
		}

		public List<Movie> GetMovies()
		{
			return _movieRepository.Get();
		}

		public void DeleteMovie(int movieChoice)
		{
			var movies = _movieRepository.Get();
			if (movieChoice < 1 || movieChoice > movies.Count)
			{
				throw new ArgumentException("Movie choice is not valid");
			}
			Movie movie = movies[movieChoice-1];
			_movieRepository.Delete(movie);
		}
	}
}
