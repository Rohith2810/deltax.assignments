﻿Feature: IMDB
	International Movie DataBase

@addActor
Scenario: Adding a Actor
	Given I have a actor with name "Ak Sai"
	And date of birth is "1999/06/15"
	When I add the actor to the Repository
	Then My Actor Repository look like this
	| Name   | DateOfBirth |
	| Ak Sai | 1999/06/15  |

@addProducer
Scenario: Adding a Producer
	Given I have a producer with name "Sri Chand"
	And date of birth is "1999/11/23"
	When I add the producer to the Repository
	Then My Producer Repository look like this
	| Name      | DateOfBirth |
	| Sri Chand | 1999/11/23  |

@addMovie
Scenario: Adding a Movie
	Given I have a movie with name "Iron Man"
	And year of release is "2008"
	And plot is "Iron Man is a 2008 American superhero film."
	And producer is "1"
	And actors are "1 2 3"
	When I add Movie to Repository
	Then My Movie Repository look like this
	| Name     | YearOfRelease | Plot                                        |
	| Iron Man | 2008          | Iron Man is a 2008 American superhero film. | 
	And My producers will be
	| Name   | DateOfBirth |
	| Marvel | 1944/11/07  |
	And My Actors will be
	| Name     | DateOfBirth |
	| Robert   | 1970/01/01  |
	| Gwyneth  | 1981/01/01  |
	| Terrance | 1973/01/01  |
@listMovie
Scenario: Listing the movies
	Given I have Repository of books
	When I fetch my movies
	Then I should have the following Movies
	| Name           | YearOfRelease | Plot                                              |
	| Iron Man       | 2008          | Iron Man is a 2008 American superhero film.       |
	| Doctor Strange | 2016          | Doctor Strange is a 2016 American superhero film. |
	And My producers will be
	| Name   | DateOfBirth |
	| Marvel | 1944/11/07  |
	| Marvel | 1944/11/07  |
	And My Actors will be
    | Name     | DateOfBirth |
    | Robert   | 1970/01/01  |
    | Gwyneth  | 1981/01/01  |
    | Terrance | 1973/01/01  |
    | Benedict | 1978/01/01  |
    | Rachel   | 1979/01/01  |
@deleteMovie
Scenario: Deleting a movie
	Given I have the movie "1"
	When I fetch my movies
	And I delete the movie
	Then My Movie Repository look like this
	| Name           | YearOfRelease | Plot                                              |
	| Doctor Strange | 2016          | Doctor Strange is a 2016 American superhero film. |
	And My producers will be
        | Name   | DateOfBirth |
        | Marvel | 1944/11/07  |
	And My Actors will be
         | Name     | DateOfBirth |
         | Benedict | 1978/01/01  |
		 | Rachel   | 1979/01/01  |