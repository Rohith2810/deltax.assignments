﻿using System;
using TechTalk.SpecFlow;
using IMDB;
using IMDB.Domain;
using System.Collections.Generic;
using TechTalk.SpecFlow.Assist;
using Xunit;
using System.Linq;

namespace IMDB.Tests
{
    [Binding]
    public class IMDBSteps
    {
        private string _actorName;
        private string _dateOfBirth;
        private string _movieName;
		private int _yearOfRelease;
		private int _producerChoice;
		private string[] _actorChoices;
		private int _movieChoice;
        private ImdbService _imdbService = new ImdbService();
		private string _producerName;
		private string _plot;
		private List<Movie> _movies;
        private List<Person> _moviesProducers;
		private List<Person> _moviesActors;

		[Given(@"I have a actor with name ""(.*)""")]
        public void GivenIHaveAActorWithName(string actorName)
        {
            _actorName = actorName;
        }

        [Given(@"I have a producer with name ""(.*)""")]
        public void GivenIHaveAProducerWithName(string producerName)
        {
            _producerName = producerName;
        }

        [Given(@"date of birth is ""(.*)""")]
        public void GivenDateOfBirthIs(string dateOfBirth)
        {
            _dateOfBirth = dateOfBirth;
        }
        
        [Given(@"I have a movie with name ""(.*)""")]
        public void GivenIHaveAMovieWithName(string movieName)
        {
            _movieName = movieName;
        }
        
        [Given(@"year of release is ""(.*)""")]
        public void GivenYearOfReleaseIs(int yearOfRelease)
        {
            _yearOfRelease = yearOfRelease;
        }

        [Given(@"plot is ""(.*)""")]
        public void GivenPlotIs(string plot)
        {
            _plot = plot;
        }

        [Given(@"producer is ""(.*)""")]
        public void GivenProducerIs(int producerChoice)
        {
            _producerChoice = producerChoice;
        }
        
        [Given(@"actors are ""(.*)""")]
        public void GivenActorsAre(string actorChoices)
        {
            _actorChoices = actorChoices.Split(' ');
        }
        
        [Given(@"I have Repository of books")]
        public void GivenIHaveRepositoryOfBooks()
        {
        }
        
        [Given(@"I have the movie ""(.*)""")]
        public void GivenIHaveTheMovie(int movieChoice)
        {
            _movieChoice = movieChoice;
        }
        
        [When(@"I add the actor to the Repository")]
        public void WhenIAddTheActorToTheRepository()
        {
            _imdbService.AddPerson(_actorName, _dateOfBirth, ImdbService.PersonType.actor);
        }
        
        [When(@"I add the producer to the Repository")]
        public void WhenIAddTheProducerToTheRepository()
        {
            _imdbService.AddPerson(_producerName, _dateOfBirth, ImdbService.PersonType.producer);
        }
                
        [When(@"I add Movie to Repository")]
        public void WhenIAddMovieToRepository()
        {
            _imdbService.AddMovie(_movieName, _yearOfRelease, _plot, _producerChoice, _actorChoices);
        }
        
        [When(@"I fetch my movies")]
        public void WhenIFetchMyMovies()
        {
            _moviesProducers = new List<Person>();
            _moviesActors = new List<Person>();
            _movies = _imdbService.GetMovies();
            foreach (var movie in _movies)
            {
                _moviesProducers.Add(movie.Producer);
                _moviesActors = _moviesActors.Concat(movie.Actors).ToList();
            }

        }
        
        [When(@"I delete the movie")]
        public void WhenIDeleteTheMovie()
        {
            _imdbService.DeleteMovie(_movieChoice);
        }
        
        [Then(@"My Actor Repository look like this")]
        public void ThenMyActorRepositoryLookLikeThis(Table table)
        {
            var actors = _imdbService.GetPersons(ImdbService.PersonType.actor);
            table.CompareToSet(actors);
        }
        
        [Then(@"My Producer Repository look like this")]
        public void ThenMyProducerRepositoryLookLikeThis(Table table)
        {
            var producers = _imdbService.GetPersons(ImdbService.PersonType.producer);
            table.CompareToSet(producers);
        }
        
        [Then(@"My Movie Repository look like this")]
        public void ThenMyMovieRepositoryLookLikeThis(Table table)
        {
            WhenIFetchMyMovies();
            table.CompareToSet(_movies);
        }



        [Then(@"I should have the following Movies")]
        public void ThenIShouldHaveTheFollowingMovies(Table table)
        {
            table.CompareToSet(_movies);
        }

        [Then(@"My producers will be")]
        public void ThenMyProducersWillBe(Table table)
        {
            table.CompareToSet(_moviesProducers);
        }

        [Then(@"My Actors will be")]
        public void ThenMyActorsWillBe(Table table)
        {
            table.CompareToSet(_moviesActors);
        }

        [BeforeScenario("addMovie","listMovie","deleteMovie")]
        public void AddSampleActorsAndProducers()
        {
            _imdbService.AddPerson("Marvel", "1944/11/07", ImdbService.PersonType.producer);
            _imdbService.AddPerson("Robert", "1970/01/01", ImdbService.PersonType.actor);
            _imdbService.AddPerson("Gwyneth", "1981/01/01", ImdbService.PersonType.actor);
            _imdbService.AddPerson("Terrance", "1973/01/01", ImdbService.PersonType.actor);
            _imdbService.AddPerson("Benedict", "1978/01/01", ImdbService.PersonType.actor);
            _imdbService.AddPerson("Rachel", "1979/01/01", ImdbService.PersonType.actor);
        }

        [BeforeScenario("listMovie","deleteMovie")]
        public void AddSampleMoivies()
        {
            _imdbService.AddMovie("Iron Man", 2008, "Iron Man is a 2008 American superhero film.", 1, new string[] { "1", "2", "3" });
            _imdbService.AddMovie("Doctor Strange", 2016, "Doctor Strange is a 2016 American superhero film.", 1, new string[] { "4", "5" });
        }

    }
        
}

