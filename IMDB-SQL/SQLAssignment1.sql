CREATE DATABASE School

USE School

--CREATE TABLE Classes (Id INT IDENTITY(1,1) PRIMARY KEY,Name VARCHAR(3),Section VARCHAR(10), Number INT)

--CREATE TABLE Teachers (Id INT IDENTITY(1,1) PRIMARY KEY,Name VARCHAR(50),DOB DATE,Gender VARCHAR(10))

--CREATE TABLE Students (Id INT IDENTITY(1,1) PRIMARY KEY, Name VARCHAR(50), DOB DATE,Gender VARCHAR(10),ClassId INT FOREIGN KEY REFERENCES Classes(Id))

--CREATE TABLE TeacherClassMapping (TeacherId INT FOREIGN KEY REFERENCES Teachers(Id),ClassId INT FOREIGN KEY REFERENCES Classes(Id))

INSERT INTO Classes values ('IX','A',201)
INSERT INTO Classes values ('IX','B',202)
INSERT INTO Classes values ('X','A',203)

INSERT INTO Teachers values ('Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teachers values ('Monica Bing','1982/03/06','Female')
INSERT INTO Teachers values ('Chandler Bing','1978/12/17','Male')
INSERT INTO Teachers values ('Ross Geller','1993/01/26','Male')

INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)

INSERT INTO TeacherClassMapping values (1,1)
INSERT INTO TeacherClassMapping values (1,2)
INSERT INTO TeacherClassMapping values (2,2)
INSERT INTO TeacherClassMapping values (2,3)
INSERT INTO TeacherClassMapping values (3,3)
INSERT INTO TeacherClassMapping values (3,1)

--Find list of male students 
SELECT * 
FROM Students 
WHERE Gender='Male'

--Find list of student older than 2005/01/01
SELECT * 
FROM Students 
WHERE DOB<'2005/01/01'

--Youngest student in school
SELECT TOP 1 *
FROM Students 
ORDER BY DOB DESC

--Find student distinct birthdays
SELECT DISTINCT DOB 
FROM Students

-- No of students in each class
SELECT C.Name,COUNT(*) AS [Count] 
FROM Classes C 
INNER JOIN Students S 
ON C.Id=S.ClassId 
GROUP BY C.Name

-- No of students in each section
SELECT C.Name, C.Section, COUNT(S.Id) AS [Count] 
FROM Classes C 
INNER JOIN Students S 
ON C.Id=S.ClassId 
GROUP BY C.Id,C.Name,C.Section

-- No of classes taught by teacher
SELECT T.Name,COUNT(TCM.ClassId) AS [Count] 
FROM TeacherClassMapping TCM 
INNER JOIN Teachers T 
ON TCM.TeacherId=T.Id 
GROUP BY T.Id,T.Name

-- List of teachers teaching Class X
SELECT T.Name,T.DOB,T.Gender 
FROM Teachers T 
INNER JOIN TeacherClassMapping TCM 
ON T.Id=TCM.TeacherId 
INNER JOIN Classes C 
ON TCM.ClassId=C.Id 
WHERE C.Name='X'

-- Classes which have more than 2 teachers teaching
SELECT C.Name,
		COUNT(DISTINCT TCM.TeacherId) AS CountOfTeachers 
FROM Classes C 
INNER JOIN TeacherClassMapping TCM 
ON C.Id=TCM.ClassId 
GROUP BY C.Name 
HAVING COUNT(DISTINCT TCM.TeacherId)>2

-- List of students being taught by 'Lisa'
SELECT S.Name 
FROM Students S 
INNER JOIN Classes C 
ON S.ClassId=C.Id 
INNER JOIN TeacherClassMapping TCM 
ON C.Id=TCM.ClassId 
INNER JOIN Teachers T 
ON TCM.TeacherId=T.Id 
WHERE T.Name LIKE 'Lisa%'