CREATE DATABASE IMDBSample

CREATE TABLE Actors (Id INT IDENTITY(1,1) PRIMARY KEY,Name VARCHAR(50),Gender VARCHAR(10),DOB DATE)

CREATE TABLE Producers (Id INT IDENTITY(1,1) PRIMARY KEY,Name VARCHAR(50),Company VARCHAR(50),CompanyEstDate DATE)

CREATE TABLE Movies (Id INT IDENTITY(1,1) PRIMARY KEY,Name VARCHAR(100),Language VARCHAR(50),ProducerId INT FOREIGN KEY REFERENCES Producers(Id),Profit INT)

CREATE TABLE ActorMoviesMapping (MovieId INT FOREIGN KEY REFERENCES Movies(Id),ActorId INT FOREIGN KEY REFERENCES Actors(Id),PRIMARY KEY(MovieId,ActorId))

INSERT INTO Actors VALUES('Mila Kunis','Female','11/14/1986')
INSERT INTO Actors VALUES('Robert DeNiro','Male','07/10/1957')
INSERT INTO Actors VALUES('George Michael','Male','11/23/1978')
INSERT INTO Actors VALUES('Mike Scott','Male','08/06/1969')
INSERT INTO Actors VALUES('Pam Halpert','Female','09/26/1996')
INSERT INTO Actors VALUES('Dame Judi Dench','Female','04/05/1947')

INSERT INTO Producers VALUES('Arjun','Fox','05/14/1998')
INSERT INTO Producers VALUES('Arun','Bull','09/11/2004')
INSERT INTO Producers VALUES('Tom','Hanks','11/03/1987')
INSERT INTO Producers VALUES('Zeshan','Male','11/14/1996')
INSERT INTO Producers VALUES('Nicole','Team Coco','09/26/1992')
INSERT INTO Producers VALUES('Priya','Team Priya','09/26/1992')

INSERT INTO Movies VALUES('Rocky','English',1,10000)
INSERT INTO Movies VALUES('Rocky','Hindi',3,3000)
INSERT INTO Movies VALUES('Terminal','English',4,300000)
INSERT INTO Movies VALUES('Rambo','Hindi',2,93000)
INSERT INTO Movies VALUES('Rudy','English',5,9600)

INSERT INTO ActorMoviesMapping VALUES(1,1)
INSERT INTO ActorMoviesMapping VALUES(1,3)
INSERT INTO ActorMoviesMapping VALUES(1,5)
INSERT INTO ActorMoviesMapping VALUES(2,6)
INSERT INTO ActorMoviesMapping VALUES(2,5)
INSERT INTO ActorMoviesMapping VALUES(2,4)
INSERT INTO ActorMoviesMapping VALUES(2,2)
INSERT INTO ActorMoviesMapping VALUES(3,3)
INSERT INTO ActorMoviesMapping VALUES(3,2)
INSERT INTO ActorMoviesMapping VALUES(4,1)
INSERT INTO ActorMoviesMapping VALUES(4,6)
INSERT INTO ActorMoviesMapping VALUES(4,3)
INSERT INTO ActorMoviesMapping VALUES(5,2)
INSERT INTO ActorMoviesMapping VALUES(5,5)
INSERT INTO ActorMoviesMapping VALUES(5,3)

--Update Profit of all the movies by +1000 where producer name contains 'run'
UPDATE Movies 
SET Profit=Profit+1000 
WHERE ProducerId IN
(SELECT Id 
FROM Producers 
WHERE Name LIKE '%run%')

--Find duplicate movies having the same name and their count
SELECT Name,
		COUNT(*) AS [COUNT]
FROM Movies
GROUP BY Name
HAVING COUNT(*)>1

--Find the oldest actor/actress for each movie
SELECT R.Name as [Movie], A.Name as [Oldest Actor/Actress]
FROM (SELECT M.Id,M.Name,MIN(A.DOB) AS MinimumDOB
FROM Movies M 
INNER JOIN ActorMoviesMapping MCM
ON M.Id=MCM.MovieId
INNER JOIN Actors A
ON MCM.ActorId=A.Id
GROUP BY M.Id,M.Name) R
INNER JOIN ActorMoviesMapping MCM
ON R.Id=MCM.MovieId
INNER JOIN Actors A
ON MCM.ActorId=A.Id
WHERE A.DOB=R.MinimumDOB

--List of producers who have not worked with actor X
SELECT P.Name FROM Producers P
LEFT JOIN
(SELECT M.ProducerId AS Id FROM Movies M
INNER JOIN ActorMoviesMapping MAM
ON M.Id=MAM.MovieId
INNER JOIN Actors A
ON MAM.ActorId=A.Id
WHERE A.Name='Robert DeNiro') R
ON P.Id=R.Id
WHERE R.Id IS NULL

--List of pair of actors who have worked together in more than 2 movies

SELECT A1.Name,A2.Name,COUNT(*)
FROM Actors A1
INNER JOIN ActorMoviesMapping MAM1
ON A1.Id=MAM1.ActorId 
INNER JOIN (Actors A2
INNER JOIN ActorMoviesMapping MAM2
ON A2.Id=MAM2.ActorId) 
ON MAM1.MovieId=MAM2.MovieId AND A1.Id>A2.Id
GROUP BY A1.Id,A1.Name,A2.Id,A2.Name
HAVING COUNT(*)>=2

--Add non-clustered index on profit column of movies table
CREATE NONCLUSTERED INDEX Movie_Profit
ON Movies(Profit)

--Create stored procedure to return list of actors for given movie id
CREATE PROCEDURE SelectAllActors @MovieId INT
AS
SELECT A.Name 
FROM Actors A
INNER JOIN ActorMoviesMapping MAM
ON A.Id=MAM.ActorId
WHERE MAM.MovieId=@MovieId
GO

EXEC SelectAllActors @MovieId=1

--Create a function to return age for given date of birth
CREATE FUNCTION CalculateAge(@DOB AS DATETIME)
RETURNS INT
AS
BEGIN
DECLARE @Age AS INT

SET @Age=DATEDIFF(YY,@DOB,GETDATE())

RETURN @AGE
END

SELECT dbo.CalculateAge(DOB)
FROM Actors

--Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 
CREATE PROCEDURE IncreaseProfit @MovieIds VARCHAR(50)
AS
UPDATE Movies 
SET Profit = Profit + 100
WHERE Id IN
(SELECT *
FROM string_split(@MovieIds,','))
GO

EXEC IncreaseProfit @MovieIds ='1,2,3'

SELECT * FROM Producers

--SELECT P.Name 
--FROM Producers P
--LEFT JOIN Movies M
--ON P.Id=M.ProducerId
--WHERE M.ProducerId IS NULL

--Companies(Id,Name,HeadQuarters,EstDate)
--Producers(Id,Name,CompanyId)

--Select M.Name, COUNT(AMM.ActorId) 
--FROM Movies M
--INNER JOIN ActorMoviesMapping AMM
--ON M.Id=AMM.MovieId
--GROUP BY M.Id,M.Name