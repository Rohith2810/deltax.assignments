CREATE PROCEDURE usp_AddMovie @Name NVARCHAR(MAX)
,@YearOfRelease INT
,@ProducerId INT
,@Plot NVARCHAR(MAX)
,@PosterURL NVARCHAR(MAX)
,@ActorIds NVARCHAR(MAX)
,@GenreIds NVARCHAR(MAX)

AS

INSERT INTO Movies
VALUES(@Name
,@YearOfRelease
,@ProducerId
,@Plot
,@PosterURL
);

DECLARE @MovieId AS INT

SET @MovieId = SCOPE_IDENTITY();

INSERT INTO MovieActorsMapping
SELECT @MovieId [MovieId]
,[value] [ActorId]
FROM STRING_SPLIT(@ActorIds,',');

INSERT INTO MovieGenresMapping
SELECT @MoiveId [MovieId]
,[value] [GenreId]
FROM STRING_SPLIT(@GenreIds,',');

SELECT @MovieId;

GO

CREATE PROCEDURE usp_UpdateMovie @MovieId INT
,@Name NVARCHAR(MAX)
,@YearOfRelease INT
,@ProducerId INT
,@Plot NVARCHAR(MAX)
,@PosterURL NVARCHAR(MAX)
,@ActorIds NVARCHAR(MAX)
,@GenreIds NVARCHAR(MAX)

AS

UPDATE Movies
SET Name=@Name
,YearOfRelease=@YearOfRelease
,ProducerId=@ProducerId
,Plot=@Plot
,PosterURL=@PosterURL
WHERE Id=@MovieId;

DECLARE @MId AS INT
SET @MId=@MovieId

DELETE
FROM MovieActorsMapping
WHERE MovieId=@MovieId;

DELETE
FROM MovieGenresMapping
WHERE MovieId=@MovieId;

INSERT INTO MovieActorsMapping
SELECT @MId [MovieId]
,[value] [ActorId]
FROM STRING_SPLIT(@ActorIds,',');

INSERT INTO MovieGenresMapping
SELECT @MId [MovieId]
,[value] [ActorId]
FROM STRING_SPLIT(@GenreIds,',');

GO

CREATE PROCEDURE usp_DeleteMovie @Id INT
AS

DELETE FROM MovieActorsMapping
WHERE MovieId=@Id;

DELETE FROM MovieGenresMapping
WHERE MovieId=@Id;

DELETE FROM Movies
WHERE Id=@Id;

GO

CREATE PROCEDURE usp_DeleteActor @Id INT
AS

DELETE FROM MovieActorsMapping
WHERE ActorId=@Id;

DELETE FROM Actors
WHERE Id=@Id;

GO

CREATE PROCEDURE usp_DeleteGenre @Id INT
AS

DELETE FROM MovieGenresMapping
WHERE GenreId=@Id;

DELETE FROM Genres
WHERE Id=@Id;

GO

CREATE PROCEDURE usp_UpdateMovieActors @Id INT
,@ActorIds NVARCHAR(MAX)
AS

DELETE
FROM MovieActorsMapping
WHERE MovieId=@Id;

INSERT INTO MovieActorsMapping
SELECT @Id [MovieId]
,[value] [ActorId]
FROM STRING_SPLIT(@ActorIds,',');

GO

CREATE PROCEDURE usp_UpdateMovieGenres @Id INT
,@GenreIds NVARCHAR(MAX)
AS

DELETE
FROM MovieGenresMapping
WHERE MovieId=@Id;

INSERT INTO MovieGenresMapping
SELECT @Id [MovieId]
,[value] [GenreId]
FROM STRING_SPLIT(@GenreIds,',');

GO