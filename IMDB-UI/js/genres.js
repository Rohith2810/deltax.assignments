$(document).ready(function(){
    $("#nav").load("navigationBar.html",function(){
        $("a[href='genres.html']").addClass("active");
    });
    $.ajax({url:"https://localhost:44373/genres",type:"GET",success:genreSuccessFunction,error:genreErrorFunction})
});

var genres=[]
function genreSuccessFunction(genres)
{
    $(".genreCards").empty();
    genres.forEach(genre=>{
        let child=`
        <div class="card col-sm-4" id="${genre.id}">
        <div class="card-body">
            <h3 class="card-title genreName">${genre.name}</h3>
        </div>
        <div class="actionButtons">
            <button class="btn deleteButton" onclick=deleteGenre(${genre.id})><span class="material-icons">delete</span></button>
            <button class="btn editButton" onclick="return editGenre(${genre.id})" data-toggle="modal" data-target="#editGenre")><span class="material-icons">edit</span></button>
        </div>
    </div>`;
    console.log("Success");
    $(".genreCards").append(child);
    });
}

function genreErrorFunction(error)
{
    console.log(error);
}

function deleteGenre(id)
{
    let choice=confirm("Are you sure to delete Genre!");
    if(choice)
    {
        $.ajax({
            url:"https://localhost:44373/genres/"+id,
            type:"DELETE",
            success:function(response){
                $(`#${response.id}`).remove();
                alert("Deleting genre successfully completed");
            }
        });
    }
    else
    {
        return;
    }
}

function editGenre(id)
{
    $.ajax({
        url:"https://localhost:44373/genres/"+id,
        type:"GET",
        success:function(response)
        {
            $("#editGenreName").val(response.name);
            $("#editGenreForm").attr("onsubmit",`putGenre(${id})`);
        },
        error:function(error)
        {
            console.log(error);
        }
    });
}

function putGenre(id)
{
    let name=$("#editGenreName").val();
    let putData=JSON.stringify({
        "name":name
    });
    $.ajax({
        url:"https://localhost:44373/genres/"+id,
        type:"PUT",
        data:putData,
        contentType: "application/json; charset=utf-8",
        success:function(response){
            $(`#${response.id} .card-title`).html(`${name}`);
            alert("Updating genre successfully completed.");
            $("#editClose").trigger("click");
        },
        error:function(error){
            console.log(error);
        }
    });
}