var ajaxRequests=[];
$(document).ready(function(){
    $("#genres").select2();
    $('#actors').select2();
    $("#actorDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#producerDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#nav").load("navigationBar.html");
    ajaxRequests.push($.ajax({
        url:"https://localhost:44373/producers",
        type:"GET",
        success:producerSuccess,
        error:producerError
    }));
    ajaxRequests.push($.ajax({
        url:"https://localhost:44373/actors",
        type:"GET",
        success:actorSuccess,
        error:actorError
    }));
    ajaxRequests.push($.ajax({
        url:"https://localhost:44373/genres",
        type:"GET",
        success:genreSuccess,
        error:genreError
    }));
    let query=window.location.search;
    let urlParams=new URLSearchParams(query);
    let id=urlParams.get('id');
    if(id==0)
        $(".title").html("Add New Movie");
    else
    {
        $(".title").html("Edit Movie");
        ajaxRequests.push($.ajax({
            url:"https://localhost:44373/movies/"+id,
            type:"GET",
            success:movieSuccess,
            error:movieError
        }));
    }
});
var producers=[];
var actors=[];
var genres=[];

function producerSuccess(response)
{
    producers=response;
    populateProducersList();
}

function producerError(error)
{
    console.log(error);
}

function populateProducersList()
{
    let childs=['<option value="_">Producer</option>'];
    producers.forEach(producer=>{
        childs.push(`<option value="${producer.id}">${producer.name}</option>`);
    });
    $("#producer").html(childs.join(''));
    console.log("success");
}

function actorSuccess(response)
{
    actors=response;
    populateActorsList();
}

function actorError(error)
{
    console.log(error);
}

function populateActorsList()
{
    let childs=[];
    actors.forEach(actor=>{
        childs.push(`<option value="${actor.id}">${actor.name}</option>`);
    });
    $("#actors").html(childs.join(" "));
    console.log("Success");
}

function genreSuccess(response)
{
    genres=response;
    populateGenresList();
}

function genreError(error)
{
    console.log(error);
}

function populateGenresList()
{
    let childs=[];
    genres.forEach(genre=>{
        childs.push(`<option value="${genre.id}">${genre.name}</option>`)
    });
    $("#genres").html(childs.join(" "));
    console.log("Success");
}

function addActor()
{
    let postData=JSON.stringify({
        "name":$("#actorName").val(),
        "bio":$("#actorBio").val(),
        "dob":$("#actorDob").val(),
        "sex":$("#actorSex").val()
    });
    $.ajax({
        url:"https://localhost:44373/actors",
        type:"POST",
        data:postData,
        contentType: "application/json; charset=utf-8",
        success: function(response)
        {
            $.ajax({
                url:"https://localhost:44373/actors/"+response.id,
                type:"GET",
                success:function(response){
                    actors.push(response);
                    $("#addActorClose").trigger("click");
                    $("#addActorClear").trigger("click");
                    populateActorsList();
                },
                error:function(error){
                    console.log(error);
                }
            });
        },
        error:function(error){
            alert("Error");
            console.log(error);
        }
    });
    alert("Adding actor successfully completed.");
}


function addProducer()
{
    let postData=JSON.stringify({
        "name":$("#producerName").val(),
        "bio":$("#producerBio").val(),
        "dob":$("#producerDob").val(),
        "sex":$("#producerSex").val()
    });
    $.ajax({
        url:"https://localhost:44373/producers",
        type:"POST",
        data:postData,
        contentType:"application/json; charset=utf-8",
        success:function(response){
            $.ajax({
                url:"https://localhost:44373/producers/"+response.id,
                type:"GET",
                success:function(response){
                    producers.push(response);
                    $("#addProducerClose").trigger("click");
                    $("#addProducerClear").trigger("click");
                    populateProducersList();
                },
                error:function(error){
                    console.log(error);
                }
            });
        },
        error:function(error){
            console.log(error);
        }
    });
    alert("Adding producer successfully completed.");
}

function addGenre()
{
    let postData=JSON.stringify({
        "name":$("#genreName").val()
    });
    $.ajax({
        url:"https://localhost:44373/genres",
        type:"POST",
        data:postData,
        contentType:"application/json; charset=utf-8",
        success:function(response){
            $.ajax({
                url:"https://localhost:44373/genres/"+response.id,
                type:"GET",
                success:function(response){
                    genres.push(response);
                    $("#addGenreClose").trigger("click");
                    $("#addGenreClear").trigger("click");
                    populateGenresList();
                },
                error:function(error){
                    console.log(error);
                }
            });
        },
        error:function(error)
        {
            console.log(error);
        }
    });
    alert("Adding genre succesfully completed.");
}

function addMovie()
{
    let producerId=$("#producer").val();
    if(producerId=='_')
    {
        alert("Choose a valid Producer");
        return false;
    }
    let formData=new FormData();
    let files=$("#poster").prop('files');
    let poster=files[0];
    formData.append("file",poster,poster.name);
    $.ajax({
        url:"https://localhost:44373/movies/upload",
        type:"POST",
        data:formData,
        contentType:false,
        processData: false,
        success:function(response){
            let postData=JSON.stringify({
                "name":$("#movieName").val(),
                "yearOfRelease":$("#yearOfRelease").val(),
                "producerId":producerId,
                "actorIds":$("#actors").val(),
                "genreIds":$("#genres").val(),
                "plot":$("#plot").val(),
                "posterURL":response
            });
            $.ajax({
                url:"https://localhost:44373/movies",
                type:"POST",
                data:postData,
                contentType:"application/json; charset=utf-8",
                success:function(response){
                    alert("Adding movie succesfully completed.");
                    location.href="index.html";
                },
                error:function(error)
                {
                    console.log(error);
                }
            });
        },
        error:function(error){
            console.log(error);
        }
    });
    
}

function movieSuccess(response)
{
    populateForm(response);
}

function movieError(error)
{
    console.log(error);
}

function populateForm(response)
{
    Promise.all(ajaxRequests).then(()=>{
        $("#movieName").val(response.name);
        $("#yearOfRelease").val(response.yearOfRelease);
        $("#producer").val(response.producer.id);
        response.actors.forEach(actor=>{
            $(`select[id="actors"] option[value="${actor.id}"]`).attr("selected","selected");
            $("#actors").trigger("change");
        });
        response.genres.forEach(genre=>{
            $(`select[id="genres"] option[value="${genre.id}"]`).attr("selected","selected");
            $("#genres").trigger("change");
        });
        $("#plot").val(response.plot);
        $("#addMovie").attr("onsubmit",`return editMovie(${response.id})`);
    });
}

function editMovie(id)
{
    let producerId=$("#producer").val();
    if(producerId=='_')
    {
        alert("Choose a valid Producer");
        return false;
    }
    let formData=new FormData();
    let files=$("#poster").prop('files');
    let poster=files[0];
    formData.append("file",poster,poster.name);
    $.ajax({
        url:"https://localhost:44373/movies/upload",
        type:"POST",
        data:formData,
        contentType:false,
        processData: false,
        success:function(response){
            let putData=JSON.stringify({
                "name":$("#movieName").val(),
                "yearOfRelease":$("#yearOfRelease").val(),
                "producerId":producerId,
                "actorIds":$("#actors").val(),
                "genreIds":$("#genres").val(),
                "plot":$("#plot").val(),
                "posterURL":response
            });
            $.ajax({
                url:"https://localhost:44373/movies/"+id,
                type:"PUT",
                data:putData,
                contentType:"application/json; charset=utf-8",
                success:function(response){
                    alert("Movie updated successfully.");
                    location.href="index.html";
                },
                error:function(error){
                    console.log(error);
                }
            });
        },
        error:function(error){
            console.log(error);
        }
    });
    
}