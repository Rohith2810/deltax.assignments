$(document).ready(function(){
    $("#actorDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#editActorDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#nav").load("navigationBar.html",function(){
        $("a[href='actors.html']").addClass("active");
    });
    $.ajax({url:"https://localhost:44373/actors",type:"GET",success:actorSuccessFunction,error:actorErrorFunction})
});
var months=["January","February","March","April","May","June","July","August","September","October","November","December"];

function actorSuccessFunction(actors)
{
    $(".actorCards").empty();
    actors.forEach(actor => {
        let date=new Date(actor.dob);
        let dateOfBirth=months[date.getMonth()]+' '+date.getDate()+', '+date.getFullYear();
        let child=`
        <div class="card col-sm-6" id="${actor.id}">
        <div class="card-body">
            <h3 class="card-title">${actor.name}</h3>
            <span class="dob">${dateOfBirth}</span><br>
            <span class="sex">${actor.sex}</span>
            <p class="card-text bio">${actor.bio}</p>
        </div>
        <div class="actionButtons">
            <button class="btn deleteButton" onclick=deleteActor(${actor.id})><span class="material-icons">delete</span></button>
            <button class="btn editButton" data-toggle="modal" data-target="#editActor" onclick=editActor(${actor.id})><span class="material-icons">edit</span></button>
        </div>
    </div>`;
    console.log("Success");
    $(".actorCards").append(child);
    });
}

function actorErrorFunction(error)
{
    console.log(error);
}

function deleteActor(id)
{
    let choice=confirm("Are you sure to delete Actor!");
    if(choice)
    {
        $.ajax({
            url:"https://localhost:44373/actors/"+id,
            type:"DELETE",
            success: function(response)
            {
                $(`#${response.id}`).remove();
            },
            error:function(error){
                console.log(error);
            }
        });
        alert("Deleting actor successfully completed.");
    }
    else
    {
        return;
    }
}

function editActor(id)
{
    $.ajax({
        url:"https://localhost:44373/actors/"+id,
        type:"GET",
        success: function(response)
        {
            $("#editActorName").val(response.name);
            $("#editActorBio").val(response.bio);
            let date=new Date(response.dob);
            let dob=`${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`;
            $("#editActorDob").val(dob);
            $("#editActorSex").val(response.sex);
            $("#editActorForm").attr("onsubmit",`return putActor(${id})`);
        },
        error: function(error)
        {
            console.log(error);
        }
    });
}

function putActor(id)
{
    let name=$("#editActorName").val();
    let bio=$("#editActorBio").val();
    let dob=$("#editActorDob").val();
    let sex=$("#editActorSex").val()
    let putData=JSON.stringify({
        "name":name,
        "bio":bio,
        "dob":dob,
        "sex":sex
    });
    $.ajax({
        url:"https://localhost:44373/actors/"+id,
        type:"PUT",
        data:putData,
        contentType: "application/json; charset=utf-8",
        success: function(response)
        {
            let date=new Date(dob);
            let dateOfBirth=months[date.getMonth()]+' '+date.getDate()+', '+date.getFullYear();
            $(`#${response.id} .card-title`).html(`${name}`);
            $(`#${response.id} .dob`).html(`${dateOfBirth}`);
            $(`#${response.id} .sex`).html(`${sex}`);
            $(`#${response.id} .bio`).html(`${bio}`);
            alert("Updating actor successfully completed.");
            $("#editClose").trigger("click");
        },
        error:function(error){
            console.log(error);
        }
    });
}