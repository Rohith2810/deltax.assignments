$(document).ready(function(){
    $("#producerDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#editProducerDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#nav").load("navigationBar.html",function(){
        $("a[href='producers.html']").addClass("active");
    });
    $.ajax({url:"https://localhost:44373/producers",type:"GET",success:producerSuccessFunction,error:producerErrorFunction});
});

var months=["January","February","March","April","May","June","July","August","September","October","November","December"];

function producerSuccessFunction(producers)
{
    $(".producerCards").empty();
    producers.forEach(producer=>{
        let date=new Date(producer.dob);
        let dateOfBirth=months[date.getMonth()]+' '+date.getDate()+', '+date.getFullYear();
        let child=`
        <div class="card col-sm-6" id="${producer.id}">
        <div class="card-body">
            <h3 class="card-title producerrName">${producer.name}</h3>
            <span class="dob">${dateOfBirth}</span><br>
            <span class="sex">${producer.sex}</span>
            <p class="card-text bio">${producer.bio}</p>
        </div>
        <div class="actionButtons">
            <button class="btn deleteButton" onclick=deleteProducer(${producer.id})><span class="material-icons">delete</span></button>
            <button class="btn editButton" onclick=editProducer(${producer.id}) data-toggle="modal" data-target="#editProducer")><span class="material-icons">edit</span></button>
        </div>
    </div>`;
    console.log("Success");
    $(".producerCards").append(child);
    });
}

function producerErrorFunction(error)
{
    console.log(error);
}

function deleteProducer(id)
{
    let choice=confirm("Are you sure to delete Producer!");
    if(choice)
    {
        $.ajax({
            url:"https://localhost:44373/producers/"+id,
            type:"DELETE",
            success:function(response){
                $(`#${response.id}`).remove();
                alert("Deleting producer successfully completed");
            },
            error:function(error){
                console.log(error);
            }
        });
    }
    else
    {
        return;
    }
}

function editProducer(id)
{
    $.ajax({
        url:"https://localhost:44373/producers/"+id,
        type:"GET",
        success:function(response){
            $("#editProducerName").val(response.name);
            $("#editProducerBio").val(response.bio);
            let date=new Date(response.dob);
            let dob=`${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`;
            $("#editProducerDob").val(dob);
            $("#editProducerSex").val(response.sex);
            $("#editProducerForm").attr("onsubmit",`return putProducer(${id})`);
        },
        error:function(error){
            console.log(error);
        }
    });
}

function putProducer(id)
{
    let name=$("#editProducerName").val();
    let bio=$("#editProducerBio").val();
    let dob=$("#editProducerDob").val();
    let sex=$("#editProducerSex").val()
    let putData=JSON.stringify({
        "name":name,
        "bio":bio,
        "dob":dob,
        "sex":sex
    });
    $.ajax({
        url:"https://localhost:44373/producers/"+id,
        type:"PUT",
        data:putData,
        contentType: "application/json; charset=utf-8",
        success: function(response)
        {
            let date=new Date(dob);
            let dateOfBirth=months[date.getMonth()]+' '+date.getDate()+', '+date.getFullYear();
            $(`#${response.id} .card-title`).html(`${name}`);
            $(`#${response.id} .dob`).html(`${dateOfBirth}`);
            $(`#${response.id} .sex`).html(`${sex}`);
            $(`#${response.id} .bio`).html(`${bio}`);
            alert("Updating producer successfully completed.");
            $("#editClose").trigger("click");
        },
        error:function(error){
            console.log(error);
        }
    });
}