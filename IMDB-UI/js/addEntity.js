function addActor()
{
    let name=$("#actorName").val();
    let bio=$("#actorBio").val();
    let dob=$("#actorDob").val();
    let sex=$("#actorSex").val();
    let postData=JSON.stringify({
        "name":name,
        "bio":bio,
        "dob":dob,
        "sex":sex
    });
    $.ajax({
        url:"https://localhost:44373/actors",
        type:"POST",
        data:postData,
        contentType: "application/json; charset=utf-8",
        success: function(response)
        {
            $.ajax({
                url:"https://localhost:44373/actors/"+response.id,
                type:"GET",
                success:function(response){
                    $("#addActorClose").trigger("click");
                    $("#addActorClear").trigger("click");
                    if(window.location.href.indexOf('actors.html')>0)
                    {
                        let actor=response;
                        let date=new Date(actor.dob);
                        let dateOfBirth=months[date.getMonth()]+' '+date.getDate()+', '+date.getFullYear();
                        let child=`
                        <div class="card col-sm-6" id="${actor.id}">
                            <div class="card-body">
                                <h3 class="card-title">${actor.name}</h3>
                                <span class="dob">${dateOfBirth}</span><br>
                                <span class="sex">${actor.sex}</span>
                                <p class="card-text bio">${actor.bio}</p>
                            </div>
                            <div class="actionButtons">
                                <button class="btn deleteButton" onclick=deleteActor(${actor.id})><span class="material-icons">delete</span></button>
                                <button class="btn editButton" data-toggle="modal" data-target="#editActor" onclick=editActor(${actor.id})><span class="material-icons">edit</span></button>
                            </div>
                        </div>`;
                        console.log("Success");
                        $(".actorCards").append(child);
                    }
                },
                error:function(error){
                    console.log(error);
                }
            });
        },
        error:function(error){
            console.log(error);
        }
    });
    alert("Adding actor successfully completed.");
}

function addGenre()
{
    let postData=JSON.stringify({
        "name":$("#genreName").val()
    });
    let res;
    $.ajax({
        url:"https://localhost:44373/genres",
        type:"POST",
        data:postData,
        contentType:"application/json; charset=utf-8",
        success:function(response){
            $.ajax({
                url:"https://localhost:44373/genres/"+response.id,
                type:"GET",
                success:function(response){
                    $("#addGenreClose").trigger("click");
                    $("#addGenreClear").trigger("click");
                    if(window.location.href.indexOf('genres.html')>0)
                    {
                        let genre=response;
                        let child=`
                        <div class="card col-sm-4" id="${genre.id}">
                            <div class="card-body">
                                <h3 class="card-title genreName">${genre.name}</h3>
                            </div>
                            <div class="actionButtons">
                                <button class="btn deleteButton" onclick=deleteGenre(${genre.id})><span class="material-icons">delete</span></button>
                                <button class="btn editButton" onclick="return editGenre(${genre.id})" data-toggle="modal" data-target="#editGenre")><span class="material-icons">edit</span></button>
                            </div>
                        </div>`;
                        console.log("Success");
                        $(".genreCards").append(child);
                    }
                },
                error:function(error){
                    console.log(error);
                }
            });
            res=response;
        },
        error:function(error)
        {
            console.log(error);
        }
    });
    alert("Adding genre succesfully completed.");
}

function addProducer()
{
    let postData=JSON.stringify({
        "name":$("#producerName").val(),
        "bio":$("#producerBio").val(),
        "dob":$("#producerDob").val(),
        "sex":$("#producerSex").val()
    });
    $.ajax({
        url:"https://localhost:44373/producers",
        type:"POST",
        data:postData,
        contentType:"application/json; charset=utf-8",
        success:function(response){
            $.ajax({
                url:"https://localhost:44373/producers/"+response.id,
                type:"GET",
                success:function(response){
                    $("#addProducerClose").trigger("click");
                    $("#addProducerClear").trigger("click");
                    if(window.location.href.indexOf('producers.html')>0)
                    {
                        let producer=response;
                        let date=new Date(producer.dob);
                        let dateOfBirth=months[date.getMonth()]+' '+date.getDate()+', '+date.getFullYear();
                        let child=`
                        <div class="card col-sm-6" id="${producer.id}">
                            <div class="card-body">
                                <h3 class="card-title producerrName">${producer.name}</h3>
                                <span class="dob">${dateOfBirth}</span><br>
                                <span class="sex">${producer.sex}</span>
                                <p class="card-text bio">${producer.bio}</p>
                            </div>
                            <div class="actionButtons">
                                <button class="btn deleteButton" onclick=deleteProducer(${producer.id})><span class="material-icons">delete</span></button>
                                <button class="btn editButton" onclick=editProducer(${producer.id}) data-toggle="modal" data-target="#editProducer")><span class="material-icons">edit</span></button>
                            </div>
                        </div>`;
                        console.log("Success");
                        $(".producerCards").append(child);
                    }
                },
                error:function(error){
                    console.log(error);
                }
            });
        },
        error:function(error){
            console.log(error);
        }
    });
    alert("Adding producer successfully completed.");
}