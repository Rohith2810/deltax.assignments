$(document).ready(function(){
    $("#actorDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#producerDob").attr("max",new Date().toISOString().split("T")[0]);
    $("#nav").load("navigationBar.html",function(){
        $("a[href='index.html']").addClass("active");
    });
    $.ajax({url:"https://localhost:44373/movies",type:"GET",success:movieSuccessFunction,error:movieErrorFunction})
})
var movies=[];
function movieSuccessFunction(movies)
{
    $('#movieCards').empty();
    movies.forEach(movie => {
        let movieGenres=[];
        movie.genres.forEach(genre=>{
            movieGenres.push(genre.name);
        });
        let movieActors=[];
        movie.actors.forEach(actor=>{
            movieActors.push(actor.name);
        });
        let child=`
        <div class="card col-sm-12" id="${movie.id}">
            <img class="card-img-top" src="${movie.posterURL}" alt="No Poster">
            <div class="card-body">
                <h3 class="card-title">${movie.name}</h3>
                <h6 class="card-subtitle text-muted year-of-release">${movie.yearOfRelease}</h6>
                <hr>
                <span class="genres">${movieGenres.join(", ")}</span>
                <p class="card-text">${movie.plot}</p>
                <span class="actors">Cast: ${movieActors.join(", ")}</span><br>
                <span class="producer">Producer: ${movie.producer.name}</span>
            </div>
            <div class="actionButtons">
                <button class="btn deleteButton" onclick=deleteMovie(${movie.id})><span class="material-icons">delete</span></button>
                <button class="btn editButton" onclick="location.href='addOrEditMovie.html?id=${movie.id}'"><span class="material-icons">edit</span></button>
            </div>
        </div>`;
        $(".movieCards").append(child);
    });
}

function movieErrorFunction(error)
{
    console.log(error);
}

function deleteMovie(id)
{
    let choice=confirm("Are you sure to delete movie!");
    if(choice)
    {
        $.ajax({
            url:"https://localhost:44373/movies/"+id,
            type:"DELETE",
            success:function(response){
                $(`#${response.id}`).remove();
                alert("Movie deleted succesfully.");
            },
            error:function(error)
            {
                console.log(error);
            }
        });
    }
    else
    {
        return;
    }
}